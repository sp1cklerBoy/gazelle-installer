QT       += core gui widgets
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_UP_TO=0x050F00

TEMPLATE = app
TARGET = minstall
CONFIG += debug_and_release warn_on strict_c++ c++17
CONFIG(release, debug|release) {
    DEFINES += NDEBUG
    QMAKE_CXXFLAGS += -flto=auto
    QMAKE_LFLAGS += -flto=auto
}
TRANSLATIONS += translations/gazelle-installer_ca.ts \
                translations/gazelle-installer_de.ts \
                translations/gazelle-installer_el.ts \
		translations/gazelle-installer_es_ES.ts \
                translations/gazelle-installer_es.ts \
		translations/gazelle-installer_fr_BE.ts \
                translations/gazelle-installer_fr.ts \
                translations/gazelle-installer_gl_ES.ts \  
                translations/gazelle-installer_hu.ts \ 
                translations/gazelle-installer_it.ts \
                translations/gazelle-installer_ja.ts \
                translations/gazelle-installer_nb.ts \
                translations/gazelle-installer_nl.ts \
                translations/gazelle-installer_pl.ts \
                translations/gazelle-installer_pt_BR.ts \
                translations/gazelle-installer_pt.ts \
                translations/gazelle-installer_ro.ts \
                translations/gazelle-installer_ru.ts \
                translations/gazelle-installer_sl.ts \
                translations/gazelle-installer_sq.ts \
                translations/gazelle-installer_sv.ts \
                translations/gazelle-installer_tr.ts \ 
                translations/gazelle-installer_uk.ts  
FORMS += meinstall.ui
HEADERS += minstall.h \
    autopart.h \
    base.h \
    bootman.h \
    checkmd5.h \
    mtreeview.h \
    oobe.h \
    passedit.h \
    swapman.h \
    version.h \
    msettings.h \
    partman.h \
    mprocess.h
SOURCES += app.cpp minstall.cpp \
    autopart.cpp \
    base.cpp \
    bootman.cpp \
    checkmd5.cpp \
    msettings.cpp \
    mtreeview.cpp \
    oobe.cpp \
    partman.cpp \
    passedit.cpp \
    mprocess.cpp \
    swapman.cpp
LIBS += -lzxcvbn

RESOURCES += \
    images.qrc
