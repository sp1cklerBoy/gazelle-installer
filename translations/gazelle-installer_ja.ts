<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ja">
<context>
    <name>AutoPart</name>
    <message>
        <location filename="../autopart.cpp" line="58"/>
        <source>Root</source>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="59"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="140"/>
        <location filename="../autopart.cpp" line="142"/>
        <source>Recommended: %1
Minimum: %2</source>
        <translation>推奨要件: %1
最小要件: %2</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="179"/>
        <source>Layout Builder</source>
        <translation>Layout Builder</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="323"/>
        <source>%1% root
%2% home</source>
        <translation>root %1%
home %2%</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="325"/>
        <source>Combined root and home</source>
        <translation>root と home の組み合わせ</translation>
    </message>
</context>
<context>
    <name>Base</name>
    <message>
        <location filename="../base.cpp" line="56"/>
        <source>Cannot access installation media.</source>
        <translation>インストール用メディアにアクセスできません。</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="156"/>
        <source>Deleting old system</source>
        <translation>以前のシステムを削除中</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="166"/>
        <source>Failed to set the system configuration.</source>
        <translation>システムの設定に失敗しました。</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="168"/>
        <source>Setting system configuration</source>
        <translation>システムの設定を行っています</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="153"/>
        <source>Failed to delete old system on destination.</source>
        <translation>指定した場所の古いシステム情報の削除に失敗しました。</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="246"/>
        <source>Copying new system</source>
        <translation>新しいシステムをコピー中</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="276"/>
        <source>Failed to copy the new system.</source>
        <translation>新しいシステムのコピーに失敗しました。</translation>
    </message>
</context>
<context>
    <name>BootMan</name>
    <message>
        <location filename="../bootman.cpp" line="277"/>
        <source>Updating initramfs</source>
        <translation>initramfs を更新中</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="128"/>
        <source>Installing GRUB</source>
        <translation>GRUB のインストール中</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="106"/>
        <source>GRUB installation failed. You can reboot to the live medium and use the GRUB Rescue menu to repair the installation.</source>
        <translation>GRUB のインストールに失敗しました。Live メディアから再起動し、GRUB の回復メニューを使用してインストールを修復できます。</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="278"/>
        <source>Failed to update initramfs.</source>
        <translation>initramfs の更新に失敗しました。</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="303"/>
        <source>System boot disk:</source>
        <translation>システム起動ディスク:</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="322"/>
        <location filename="../bootman.cpp" line="332"/>
        <source>Partition to use:</source>
        <translation>使用するパーティション:</translation>
    </message>
</context>
<context>
    <name>CheckMD5</name>
    <message>
        <location filename="../checkmd5.cpp" line="38"/>
        <source>Checking installation media.</source>
        <translation>インストール用メディアを確認しています。</translation>
    </message>
    <message>
        <location filename="../checkmd5.cpp" line="39"/>
        <source>Press ESC to skip.</source>
        <translation>ESC キーを押すとスキップします。</translation>
    </message>
    <message>
        <location filename="../checkmd5.cpp" line="56"/>
        <source>The installation media is corrupt.</source>
        <translation>インストール用メディアが破損しています。</translation>
    </message>
    <message>
        <location filename="../checkmd5.cpp" line="120"/>
        <source>Are you sure you want to skip checking the installation media?</source>
        <translation>本当にインストール用メディアのチェックを省略しますか？</translation>
    </message>
</context>
<context>
    <name>MInstall</name>
    <message>
        <location filename="../minstall.cpp" line="87"/>
        <source>Shutdown</source>
        <translation>シャットダウン</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="142"/>
        <source>You are running 32bit OS started in 64 bit UEFI mode, the system will not be able to boot unless you select Legacy Boot or similar at restart.
We recommend you quit now and restart in Legacy Boot

Do you want to continue the installation?</source>
        <translation>32bit OS を 64bit UEFIモードで起動している場合、再起動時にレガシーブートかそれと同等なものを選択しないと起動できません。
今すぐ終了して、レガシーブートで再起動することをお勧めします。

インストールを続行しますか？</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="186"/>
        <source>Support %1

%1 is supported by people like you. Some help others at the support forum - %2, or translate help files into different languages, or make suggestions, write documentation, or help test new software.</source>
        <translation>&lt;p&gt;%1 のサポート

%1 はあなたのような皆さんに支えられています。サポートフォーラム %2  で他の人を助けたり、ヘルプを他の言語へ翻訳したり、あるいは提案や文書作成、新規ソフトウェアのテストを行うとうような支援が可能です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="226"/>
        <source>%1 is an independent Linux distribution based on Debian Stable.

%1 uses some components from MEPIS Linux which are released under an Apache free license. Some MEPIS components have been modified for %1.

Enjoy using %1</source>
        <translation>%1 は Debian 安定版ベースの独自 Linux ディストリビューションです

%1 は、Apache フリーライセンス下で公開されている MEPIS Linux からいくつかのコンポーネントを使用しています。いくつかの MEPIS コンポーネントは、%1 用に変更されています。

%1 をお楽しみください。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="344"/>
        <source>Pretending to install %1</source>
        <translation>%1 をインストールするふりをしています</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="362"/>
        <source>Preparing to install %1</source>
        <translation>%1 のインストールを準備中です</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="383"/>
        <source>Paused for required operator input</source>
        <translation>必要なオペレーター入力のため一時停止</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="395"/>
        <source>Setting system configuration</source>
        <translation>システムの設定を行っています</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="407"/>
        <source>Cleaning up</source>
        <translation>クリーニング中</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="411"/>
        <source>Finished</source>
        <translation>完了</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="435"/>
        <source>The installation was aborted.</source>
        <translation>インストールを中止しました。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="521"/>
        <source>Invalid settings found in configuration file (%1). Please review marked fields as you encounter them.</source>
        <translation>設定ファイル (%1) に無効な設定値が検出されました。検出された箇所を見直してください。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="541"/>
        <source>OK to format and use the entire disk (%1) for %2?</source>
        <translation>%2 用にディスク全体 (%1) をフォーマットして利用しますか？</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="545"/>
        <source>WARNING: The selected drive has a capacity of at least 2TB and must be formatted using GPT. On some systems, a GPT-formatted disk will not boot.</source>
        <translation>警告: 選択したドライブは少なくとも 2TB の容量があるので GPT を使ってフォーマットする必要があります。いくつかのシステムでは GPT でフォーマットされたディスクは起動しません。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="574"/>
        <source>The data in /home cannot be preserved because the required information could not be obtained.</source>
        <translation>/home にあるデータは保存されません。なぜなら必要な情報を入手できないからです。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="601"/>
        <source>The home directory for %1 already exists.</source>
        <translation>%1 用の home ディレクトリはすでに存在します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="637"/>
        <source>General Instructions</source>
        <translation>全般的な説明</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="638"/>
        <source>BEFORE PROCEEDING, CLOSE ALL OTHER APPLICATIONS.</source>
        <translation>作業を進めるには、他の全てのアプリを終了します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="639"/>
        <source>On each page, please read the instructions, make your selections, and then click on Next when you are ready to proceed. You will be prompted for confirmation before any destructive actions are performed.</source>
        <translation>各ページで、その説明を読んでから選択し、準備が整ってから [次へ] をクリックして先へ進んでください。シムテムに悪影響を及ぼすようなアクションが実行されるときは、あなたに確認を促します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="641"/>
        <source>Limitations</source>
        <translation>制限事項</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="642"/>
        <source>Remember, this software is provided AS-IS with no warranty what-so-ever. It is solely your responsibility to backup your data before proceeding.</source>
        <translation>このソフトウェアは現状のままで提供され、一切の保証はありません。作業を続行する前にデータをバックアップするのは全てユーザーの責任です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="647"/>
        <source>Installation Options</source>
        <translation>インストールのオプション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="648"/>
        <source>If you are running Mac OS or Windows OS (from Vista onwards), you may have to use that system&apos;s software to set up partitions and boot manager before installing.</source>
        <translation>もしあなたが Mac OS や Windows OS (Vista 以降)を稼働させているのであれば、インストールする前にそのシステム用のパーティション設定ツールや boot マネージャーを使う方が良いかもしれません。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="649"/>
        <source>Using the root-home space slider</source>
        <translation>root-home スペース・スライダーの利用</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="650"/>
        <source>The drive can be divided into separate system (root) and user data (home) partitions using the slider.</source>
        <translation>スライダを使用して、ドライブを個別のシステム (root) パーティションとユーザーデータ (home) パーティションに分割できます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="651"/>
        <source>The &lt;b&gt;root&lt;/b&gt; partition will contain the operating system and applications.</source>
        <translation>&lt;b&gt;root&lt;/b&gt; パーティションには、オペレーティングシステムとアプリケーションが含まれます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="652"/>
        <source>The &lt;b&gt;home&lt;/b&gt; partition will contain the data of all users, such as their settings, files, documents, pictures, music, videos, etc.</source>
        <translation>&lt;b&gt;home&lt;/b&gt; パーティションには、すべてのユーザの設定、ファイル、文書、画像、音楽、ビデオなどのデータが保存されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="653"/>
        <source>Move the slider to the right to increase the space for &lt;b&gt;root&lt;/b&gt;. Move it to the left to increase the space for &lt;b&gt;home&lt;/b&gt;.</source>
        <translation>スライダーを右に動かすと、&lt;b&gt;root&lt;/b&gt;の容量が増えます。左に動かすと&lt;b&gt;home&lt;/b&gt;の容量が増えます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="654"/>
        <source>Move the slider all the way to the right if you want both root and home on the same partition.</source>
        <translation>root と home の両方を同じパーティションに配置したい場合、スライダーを右いっぱいへ動かします。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="655"/>
        <source>Keeping the home directory in a separate partition improves the reliability of operating system upgrades. It also makes backing up and recovery easier. This can also improve overall performance by constraining the system files to a defined portion of the drive.</source>
        <translation>home ディレクトリを別のパーティションにしておくと、OSのアップグレード時の信頼性が向上します。また、バックアップやリカバリーも容易になります。また、システムファイルをドライブ内の特定の場所に限定することで、全体的なパフォーマンスを向上させることができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="657"/>
        <location filename="../minstall.cpp" line="741"/>
        <source>Encryption</source>
        <translation>暗号化</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="658"/>
        <location filename="../minstall.cpp" line="742"/>
        <source>Encryption is possible via LUKS. A password is required.</source>
        <translation>暗号化は LUKS で行えます。パスワードが必要です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="659"/>
        <location filename="../minstall.cpp" line="743"/>
        <source>A separate unencrypted boot partition is required.</source>
        <translation>暗号化されていない別の boot パーティションが必要です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="660"/>
        <source>When encryption is used with autoinstall, the separate boot partition will be automatically created.</source>
        <translation>自動インストール時に暗号化を利用している場合、別の boot パーティションが自動的に生成されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="661"/>
        <source>Using a custom disk layout</source>
        <translation>独自のディスクレイアウトを使用する</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="662"/>
        <source>If you need more control over where %1 is installed to, select &quot;&lt;b&gt;%2&lt;/b&gt;&quot; and click &lt;b&gt;Next&lt;/b&gt;. On the next page, you will then be able to select and configure the storage devices and partitions you need.</source>
        <translation>%1 のインストール先をより細かく管理する必要がある場合は、&lt;b&gt;%2&lt;/b&gt; を選択して &lt;b&gt;次へ&lt;/b&gt; をクリックします。次のページでは、必要なストレージデバイスとパーティションを選択して構成することができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="669"/>
        <source>Choose Partitions</source>
        <translation>パーティションを選択</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="670"/>
        <source>The partition list allows you to choose what partitions are used for this installation.</source>
        <translation>パーティションリストから、このインストールで使用するパーティションを選択することができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="671"/>
        <source>&lt;i&gt;Device&lt;/i&gt; - This is the block device name that is, or will be, assigned to the created partition.</source>
        <translation>&lt;i&gt;デバイス&lt;/i&gt; - これは作成されたパーティションに割り当てられている、または割り当てられる予定のブロックデバイス名です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="672"/>
        <source>&lt;i&gt;Size&lt;/i&gt; - The size of the partition. This can only be changed on a new layout.</source>
        <translation>&lt;i&gt;サイズ&lt;/i&gt; - パーティションのサイズです。これは、新しいレイアウトでのみ変更できます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="673"/>
        <source>&lt;i&gt;Use For&lt;/i&gt; - To use this partition in an installation, you must select something here.</source>
        <translation>&lt;i&gt;Use For&lt;/i&gt; - インストールでこのパーティションを使用するには、ここで何かを選択する必要があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="688"/>
        <source>In addition to the above, you can also type your own mount point. Custom mount points must start with a slash (&quot;/&quot;).</source>
        <translation>上記に加えて、独自のマウントポイントを入力することもできます。カスタム・マウントポイントはスラッシュ (&quot;/&quot;) で始める必要があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="689"/>
        <source>&lt;i&gt;Label&lt;/i&gt; - The label that is assigned to the partition once it has been formatted.</source>
        <translation>&lt;i&gt;ラベル&lt;/i&gt; - パーティションがフォーマットされた後に割り当てられるラベルです。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="690"/>
        <source>&lt;i&gt;Encrypt&lt;/i&gt; - Use LUKS encryption for this partition. The password applies to all partitions selected for encryption.</source>
        <translation>&lt;i&gt;Encrypt&lt;/i&gt; - このパーティションにはLUKS暗号化を使用します。パスワードは、暗号化用に選択されたすべてのパーティションに適用されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="691"/>
        <source>&lt;i&gt;Format&lt;/i&gt; - This is the partition&apos;s format. Available formats depend on what the partition is used for. When working with an existing layout, you may be able to preserve the format of the partition by selecting &lt;b&gt;Preserve&lt;/b&gt;.</source>
        <translation>&lt;i&gt;フォーマット&lt;/i&gt; - これはパーティションのフォーマットです。利用可能なフォーマットは、パーティションの使用目的によって異なります。既存のレイアウトで作業する場合、&lt;b&gt;保存&lt;/b&gt; を選択することで、そのパーティションのフォーマットを維持できる場合があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="695"/>
        <source>The ext2, ext3, ext4, jfs, xfs and btrfs Linux filesystems are supported and ext4 is recommended.</source>
        <translation>ext2、ext3、 ext4、 jfs、 xfs、 btrfs の Linux ファイルシステムをサポートしますが、 ext4 システムをお推めします。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="696"/>
        <source>&lt;i&gt;Check&lt;/i&gt; - Check and correct for bad blocks on the drive (not supported for all formats). This is very time consuming, so you may want to skip this step unless you suspect that your drive has bad blocks.</source>
        <translation>&lt;i&gt;チェック&lt;/i&gt; - ドライブ上の不正なブロックをチェックして修正します (すべてのフォーマットでサポートされているわけではありません)。これは非常に時間がかかるので、ドライブに不正なブロックがあると思われない限り、この手順は省略可能です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="698"/>
        <source>&lt;i&gt;Mount Options&lt;/i&gt; - This specifies mounting options that will be used for this partition.</source>
        <translation>&lt;i&gt;マウントオプション&lt;/i&gt; - このパーティションで使用するマウントオプションを指定します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="699"/>
        <source>&lt;i&gt;Dump&lt;/i&gt; - Instructs the dump utility to include this partition in the backup.</source>
        <translation>&lt;i&gt;Dump&lt;/i&gt; - このパーティションをバックアップに含めるように、Dump ユーティリティに指示します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="700"/>
        <source>&lt;i&gt;Pass&lt;/i&gt; - The sequence in which this file system is to be checked at boot. If zero, the file system is not checked.</source>
        <translation>&lt;i&gt;Pass&lt;/i&gt; - このファイルシステムが起動時にチェックされる順序です。0 の場合、ファイルシステムはチェックされません。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="701"/>
        <source>Menus and actions</source>
        <translation>メニューと操作</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="702"/>
        <source>A variety of actions are available by right-clicking any drive or partition item in the list.</source>
        <translation>リスト内のドライブやパーティションの項目を右クリックすると、様々な操作が可能です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="703"/>
        <source>The buttons to the right of the list can also be used to manipulate the entries.</source>
        <translation>リストの右側にあるボタンを使っても、エントリーを操作することもできます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="704"/>
        <source>The installer cannot modify the layout already on the drive. To create a custom layout, mark the drive for a new layout with the &lt;b&gt;New layout&lt;/b&gt; menu action or button (%1). This clears the existing layout.</source>
        <translation>インストーラは、ドライブ上の既存レイアウトを変更することはできません。独自のレイアウトを作成するには、&lt;b&gt;新規レイアウト&lt;/b&gt; メニューアクションまたはボタン (%1) でドライブを新規レイアウト用にマークします。これにより、既存レイアウトが消去されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="707"/>
        <source>Basic layout requirements</source>
        <translation>基本的なレイアウト要件</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="708"/>
        <source>%1 requires a root partition. The swap partition is optional but highly recommended. If you want to use the Suspend-to-Disk feature of %1, you will need a swap partition that is larger than your physical memory size.</source>
        <translation>%1 には root パーティションが必要です。 swap パーティションは任意ですが強く推奨します。 Suspend-to-Disk という機能 %1 を利用するには、物理的メモリの容量以上の swap パーティションが必要となります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="710"/>
        <source>If you choose a separate /home partition it will be easier for you to upgrade in the future, but this will not be possible if you are upgrading from an installation that does not have a separate home partition.</source>
        <translation>独立した /home パーティションを選択すると、将来のアップグレードが容易になりますが、独立した home パーティションを有しないインストールからアップグレードする場合には、これは不可能になります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="712"/>
        <source>Active partition</source>
        <translation>アクティブパーティション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="713"/>
        <source>For the installed operating system to boot, the appropriate partition (usually the boot or root partition) must be the marked as active.</source>
        <translation>インストールされたオペレーティングシステムを起動するには、適切なパーティション (通常は boot パーティションまたは root パーティション) がアクティブとしてマークされている必要があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="714"/>
        <source>The active partition of a drive can be chosen using the &lt;b&gt;Active partition&lt;/b&gt; menu action.</source>
        <translation>ドライブのアクティブパーティションは、&lt;b&gt;アクティブパーティション&lt;/b&gt;のメニューアクションを使用して選択できます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="715"/>
        <source>A partition with an asterisk (*) next to its device name is, or will become, the active partition.</source>
        <translation>デバイス名の横にアスタリスク (*) が付いているパーティションは、現在アクティブパーティション、もしくは今後アクティブパーティションになります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="720"/>
        <source>Boot partition</source>
        <translation>Boot パーティション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="721"/>
        <source>This partition is generally only required for root partitions on virtual devices such as encrypted, LVM or software RAID volumes.</source>
        <translation>このパーティションは通常、暗号化された LVM ボリュームやソフトウェア RAID ボリュームなどの、仮想デバイス上の root パーティションにのみ必要です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="722"/>
        <source>It contains a basic kernel and drivers used to access the encrypted disk or virtual devices.</source>
        <translation>暗号化されたディスクや仮想デバイスにアクセスするための、基本カーネルとドライバが含まれています。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="723"/>
        <source>BIOS-GRUB partition</source>
        <translation>BIOS-GRUB パーティション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="724"/>
        <source>When using a GPT-formatted drive on a non-EFI system, a 1MB BIOS boot partition is required when using GRUB.</source>
        <translation>GPT フォーマットのドライブを非 EFI システムで使用する場合、GRUB を使用時に 1MB の BIOS ブートパーティションが必要です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="725"/>
        <source>New drives are formatted in GPT if more than 4 partitions are to be created, or the drive has a capacity greater than 2TB. If the installer is about to format the disk in GPT, and there is no BIOS-GRUB partition, a warning will be displayed before the installation starts.</source>
        <translation>新しいドライブは、4 つ以上のパーティションを作成する場合や、ドライブの容量が 2TB を超える場合は、GPT で初期化されます。インストーラがディスクを GPT で初期化しようとしている時、BIOS-GRUB パーティションが存在しない場合、インストール開始前に警告が表示されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="727"/>
        <source>Need help creating a layout?</source>
        <translation>レイアウト作成でお困りですか？</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="728"/>
        <source>Just right-click on a drive and select &lt;b&gt;Layout Builder&lt;/b&gt; from the menu. This can create a layout similar to that of the regular install.</source>
        <translation>ドライブ上で右クリックし、メニューから &lt;b&gt;Layout Builder&lt;/b&gt; を選択してください。これで通常のインストールの場合と同様のレイアウトを作成できます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="762"/>
        <source>Install GRUB for Linux and Windows</source>
        <translation>Linux および Windows 向けに GRUB をインストール</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="763"/>
        <source>%1 uses the GRUB bootloader to boot %1 and Microsoft Windows.</source>
        <translation> %1 は GRUB ブートローダを使用して、%1 と Microsoft Windows を起動します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="764"/>
        <source>By default GRUB is installed in the Master Boot Record (MBR) or ESP (EFI System Partition for 64-bit UEFI boot systems) of your boot drive and replaces the boot loader you were using before. This is normal.</source>
        <translation>既定では、GRUB は起動ドライブのマスターブートレコード（MBR）またはESP（64ビット UEFI ブートシステム用の EFIシステムパーティション）にインストールされ、以前使用していたブートローダーと置き換わります。これは正常な動作です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="765"/>
        <source>If you choose to install GRUB to Partition Boot Record (PBR) instead, then GRUB will be installed at the beginning of the specified partition. This option is for experts only.</source>
        <translation>もし代わりに GRUB をパーティションブートレコード（PBR）にインストールすることを選択した場合は、GRUB を指定したパーティションの先頭にインストールします。このオプションは上級者向けです。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="766"/>
        <source>If you uncheck the Install GRUB box, GRUB will not be installed at this time. This option is for experts only.</source>
        <translation>「GRUB のインストール」という項目でチェックを外した場合には、GRUB は今回の作業でインストールしません。このオプションは上級者向けです。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="767"/>
        <source>Create a swap file</source>
        <translation>スワップ（swap）ファイルを作成する</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="768"/>
        <source>A swap file is more flexible than a swap partition; it is considerably easier to resize a swap file to adapt to changes in system usage.</source>
        <translation>スワップファイルは、スワップパーティションよりもずっと柔軟性があります。システムの使用状況の変化に合わせて、スワップファイルの大きさを簡単に変更することができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="769"/>
        <source>By default, this is checked if no swap partitions have been set, and unchecked if swap partitions are set. This option should be left untouched, and is for experts only.</source>
        <translation>既定では、スワップパーティションを設定していない場合はチェックが入り、設定したる場合はチェックが外れます。このオプションは上級者向けであり、なるべく変更しないようにしてください。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="770"/>
        <source>Setting the size to 0 has the same effect as unchecking this option.</source>
        <translation>サイズを 0 に設定すると、オプションのチェックを外したのと同じ効果をもたらします。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="877"/>
        <source>Enjoy using %1</source>
        <translation>%1 を上手く活用してください</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="729"/>
        <source>Upgrading</source>
        <translation>アップグレード中</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="675"/>
        <source>Format without mounting</source>
        <translation>マウントしないでフォーマット実行</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="676"/>
        <source>BIOS Boot GPT partition for GRUB</source>
        <translation>GRUB 用の BIOS 起動 GPT パーティション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="677"/>
        <location filename="../minstall.cpp" line="716"/>
        <source>EFI System Partition</source>
        <translation>EFI システムパーティション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="679"/>
        <source>Boot manager</source>
        <translation>ブートマネージャ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="680"/>
        <source>System root</source>
        <translation>システムの root</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="681"/>
        <source>User data</source>
        <translation>ユーザーデータ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="682"/>
        <source>Static data</source>
        <translation>静的データ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="683"/>
        <source>Variable data</source>
        <translation>可変データ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="684"/>
        <source>Temporary files</source>
        <translation>一時データ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="685"/>
        <source>Swap files</source>
        <translation>スワップファイル</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="686"/>
        <source>Swap partition</source>
        <translation>スワップパーティション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="693"/>
        <source>Selecting &lt;b&gt;Preserve /home&lt;/b&gt; for the root partition preserves the contents of the /home directory, deleting everything else. This option can only be used when /home is on the same partition as the root partition.</source>
        <translation>root パーティションで &lt;b&gt;/home を保存&lt;/b&gt; を選択すると、/home ディレクトリの内容が保存され、それ以外はすべて削除されます。このオプションは、/home が root パーティションと同じパーティションにある場合にのみ使用できます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="717"/>
        <source>If your system uses the Extensible Firmware Interface (EFI), a partition known as the EFI System Partition (ESP) is required for the system to boot.</source>
        <translation>システムが EFI（Extensible Firmware Interface）を使用している場合、システムが起動するには EFI システムパーティション（ESP）と呼ばれるパーティションが必要です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="718"/>
        <source>These systems do not require any partition marked as Active, but instead require a partition formatted with a FAT file system, marked as an ESP.</source>
        <translation>これらのシステムは、アクティブとマークされたパーティションを必要としません。代わりに ESP とマークされた、FAT ファイルシステムでフォーマット済みのパーティションを必要とします。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="719"/>
        <source>Most systems built within the last 10 years use EFI.</source>
        <translation>過去10年以内に作製されたシステムのほとんどは EFI を使用しています。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="730"/>
        <source>To upgrade from an existing Linux installation, select the same home partition as before and select &lt;b&gt;Preserve&lt;/b&gt; as the format.</source>
        <translation>既存の Linux インストールからアップグレードするには、以前と同じホームパーティションを選択し、フォーマットとして&lt;b&gt;保持&lt;/b&gt;を選択します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="731"/>
        <source>If you do not use a separate home partition, select &lt;b&gt;Preserve /home&lt;/b&gt; on the root file system entry to preserve the existing /home directory located on your root partition. The installer will only preserve /home, and will delete everything else. As a result, the installation will take much longer than usual.</source>
        <translation>個別の home パーティションを使用しない場合は、root ファイルシステムのエントリで &lt;b&gt;/home の保持&lt;/b&gt;を選択して、root パーティションにある既存の /home ディレクトリを保持します。インストーラは /home だけを保持し、それ以外はすべて削除します。その結果、インストールには通常の場合よりもずっと時間がかかります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="733"/>
        <source>Preferred Filesystem Type</source>
        <translation> 優先するファイルシステムのタイプ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="734"/>
        <source>For %1, you may choose to format the partitions as ext2, ext3, ext4, f2fs, jfs, xfs or btrfs.</source>
        <translation>%1 については、フォーマットするパーティションの形式を ext2、ext3、 ext4、 f2fs、 jfs、 xfs、 btrfs の中から選択することが可能です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="735"/>
        <source>Additional compression options are available for drives using btrfs. Lzo is fast, but the compression is lower. Zlib is slower, with higher compression.</source>
        <translation>btrfs を使用するドライブでは、追加の圧縮オプションを使用できます。 Lzo は高速ですが圧縮率は低くなります。 Zlib は低速ですが圧縮率が高くなります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="737"/>
        <source>System partition management tool</source>
        <translation>システムパーティション管理ツール</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="738"/>
        <source>For more control over the drive layouts (such as modifying the existing layout on a disk), click the partition management button (%1). This will run the operating system&apos;s partition management tool, which will allow you to create the exact layout you need.</source>
        <translation>ドライブのレイアウトをより詳細に管理したい場合（ディスク上の既存レイアウトを変更する場合など）は、パーティション管理ボタン (%1) をクリックします。これによって、オペレーティングシステムのパーティション管理ツールが起動し、必要なレイアウトを正確に作成することができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="744"/>
        <source>To preserve an encrypted partition, right-click on it and select &lt;b&gt;Unlock&lt;/b&gt;. In the dialog that appears, enter a name for the virtual device and the password. When the device is unlocked, the name you chose will appear under &lt;i&gt;Virtual Devices&lt;/i&gt;, with similar options to that of a regular partition.</source>
        <translation>暗号化されたパーティションを保持するには、そのパーティションを右クリックして&lt;b&gt;ロック解除&lt;/b&gt;を選択します。表示されるダイアログで、仮想デバイスの名前とパスワードを入力します。デバイスのロックが解除されると、選択した名前が&lt;i&gt;仮想デバイス&lt;/i&gt;に表示され、通常のパーティションと同様のオプションが表示されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="746"/>
        <source>For the encrypted partition to be unlocked at boot, it needs to be added to the crypttab file. Use the &lt;b&gt;Add to crypttab&lt;/b&gt; menu action to do this.</source>
        <translation>暗号化されたパーティションをブート時にロック解除するには、そのパーティションを crypttab ファイルに追加する必要があります。これを行うには、&lt;b&gt;Add to crypttab&lt;/b&gt; メニューアクションを使用します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="747"/>
        <source>Other partitions</source>
        <translation>その他のパーテーション</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="748"/>
        <source>The installer allows other partitions to be created or used for other purposes, however be mindful that older systems cannot handle drives with more than 4 partitions.</source>
        <translation>インストーラでは、他のパーティションを作成したり、他の目的に使用することができますが、古いシステムでは4つ以上のパーティションを持つドライブを処理できないことに注意してください。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="749"/>
        <source>Subvolumes</source>
        <translation>サブボリューム</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="750"/>
        <source>Some file systems, such as Btrfs, support multiple subvolumes in a single partition. These are not physical subdivisions, and so their order does not matter.</source>
        <translation>Btrfs などの一部のファイルシステムは、1 つのパーティション内で複数のサブボリュームをサポートします。これらは物理的な分割ではないため、順序は関係ありません。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="752"/>
        <source>Use the &lt;b&gt;Scan subvolumes&lt;/b&gt; menu action to search an existing Btrfs partition for subvolumes. To create a new subvolume, use the &lt;b&gt;New subvolume&lt;/b&gt; menu action.</source>
        <translation>&lt;b&gt;サブボリュームのスキャン&lt;/b&gt;メニューアクションを使用して、既存の Btrfs パーティションでサブボリュームを検索します。新しいサブボリュームを作成するには、&lt;b&gt;新規サブボリューム&lt;/b&gt;のメニューアクションを使用します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="754"/>
        <source>Existing subvolumes can be preserved, however the name must remain the same.</source>
        <translation>既存のサブボリュームは保持できますが、名前は元と同じままにする必要があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="755"/>
        <source>Virtual Devices</source>
        <translation>仮想デバイス</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="756"/>
        <source>If the intaller detects any virtual devices such as opened LUKS partitions, LVM logical volumes or software-based RAID volumes, they may be used for the installation.</source>
        <translation>イントーラーが、開いている LUK パーティションや LVM 論理ボリューム、またはソフトウェアベースの RAID ボリュームなどの仮想デバイスを検出した場合、それらはインストールで使用可能です。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="757"/>
        <source>The use of virtual devices (beyond preserving encrypted file systems) is an advanced feature. You may have to edit some files (eg. initramfs, crypttab, fstab) to ensure the virtual devices used are created upon boot.</source>
        <translation>仮想デバイスの使用 (暗号化されたファイルシステムの保持以上の操作) は高度な機能です。使用する仮想デバイスがブート時に確実に作成されるようにするには、いくつかのファイル (initramfs、crypttab、fstab など) を編集する必要があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="776"/>
        <source>&lt;p&gt;&lt;b&gt;Common Services to Enable&lt;/b&gt;&lt;br/&gt;Select any of these common services that you might need with your system configuration and the services will be started automatically when you start %1.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;コモンサービスの有効&lt;/b&gt;&lt;br/&gt;ご使用のシステム構成に必要なサービスがあればそれを選択して下さい。これは %1 起動時に自動的に開始されます。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="780"/>
        <source>&lt;p&gt;&lt;b&gt;Computer Identity&lt;/b&gt;&lt;br/&gt;The computer name is a common unique name which will identify your computer if it is on a network. The computer domain is unlikely to be used unless your ISP or local network requires it.&lt;/p&gt;&lt;p&gt;The computer and domain names can contain only alphanumeric characters, dots, hyphens. They cannot contain blank spaces, start or end with hyphens&lt;/p&gt;&lt;p&gt;The SaMBa Server needs to be activated if you want to use it to share some of your directories or printer with a local computer that is running MS-Windows or Mac OSX.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;コンピュータ ID&lt;/b&gt;&lt;br/&gt;コンピュータ名は、ネットワーク上でコンピュータを特定するための名称です。インターネットサービスプロバイダ（ISP）またはローカル・ネットワークが必要としない限り、コンピュータドメインは使われません。&lt;/p&gt;&lt;p&gt;コンピュータ名・ドメイン名は英数・ドット（.）・ハイフン（-）の文字で構成されます。空白を含めず、ハイフンは前後に付けられません。&lt;/p&gt;&lt;p&gt;フォルダやプリンターを Windows・Mac OS X を実行するコンピュータを共有するためには SAMBA サーバを有効にする必要があります。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="790"/>
        <source>Localization Defaults</source>
        <translation>既定のロケール</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="791"/>
        <source>Set the default locale. This will apply unless they are overridden later by the user.</source>
        <translation>既定の言語と地域を設定します。これは、後でユーザーが上書きしない限り、適用されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="792"/>
        <source>Configure Clock</source>
        <translation>時刻の設定</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="793"/>
        <source>If you have an Apple or a pure Unix computer, by default the system clock is set to Greenwich Meridian Time (GMT) or Coordinated Universal Time (UTC). To change this, check the &quot;&lt;b&gt;System clock uses local time&lt;/b&gt;&quot; box.</source>
        <translation>Apple または純粋な Unixコンピュータの場合、デフォルトではシステムクロックはグリニッジ標準時（GMT）または協定世界時（UTC）に設定されています。これを変更するには、「&lt;b&gt;システムクロックは現地時刻を使う&lt;/b&gt;」のボックスをチェックします。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="795"/>
        <source>The system boots with the timezone preset to GMT/UTC. To change the timezone, after you reboot into the new installation, right click on the clock in the Panel and select Properties.</source>
        <translation>システムは、時刻帯が GMT/UTC に予めセットされた状態で起動します。時刻帯を変更するには、新規インストール環境で再起動した後、パネル内の時計を右クリックし、プロパティを選択します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="797"/>
        <source>Service Settings</source>
        <translation>各種サービスの設定</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="798"/>
        <source>Most users should not change the defaults. Users with low-resource computers sometimes want to disable unneeded services in order to keep the RAM usage as low as possible. Make sure you know what you are doing!</source>
        <translation>たいていのユーザーはデフォルト値を変更しないでください。ただし、リソースの少ないコンピューターでは、RAMの使用量をできるだけ少なくするために、必要のないサービスを無効にしたい場合があります。自分が何をしようとしているのか良く考えましょう。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="804"/>
        <source>Default User Login</source>
        <translation>既定のユーザーログイン</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="805"/>
        <source>The root user is similar to the Administrator user in some other operating systems. You should not use the root user as your daily user account. Please enter the name for a new (default) user account that you will use on a daily basis. If needed, you can add other user accounts later with %1 User Manager.</source>
        <translation>root ユーザーは、他のいくつかのオペレーティングシステムにおける管理者アカウントに似ています。通常使用するユーザーアカウントとして root ユーザーを使用しないでください。日常的に使用する新しい (既定の) ユーザー アカウントの名前を入力してください。もし必要な場合は、あとから %1 ユーザー管理 でアカウントの追加を行うことができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="809"/>
        <source>Passwords</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="810"/>
        <source>Enter a new password for your default user account and for the root account. Each password must be entered twice.</source>
        <translation>既定のユーザーアカウントと root アカウントの新規パスワードを入力してください。パスワードは、それぞれ 2回入力する必要があります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="812"/>
        <source>No passwords</source>
        <translation>パスワードを使用しない</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="813"/>
        <source>If you want the default user account to have no password, leave its password fields empty. This allows you to log in without requiring a password.</source>
        <translation>既定のユーザーアカウントにパスワードを設定したくない場合は、そのパスワード入力欄を空白にしてください。これで、パスワードを要求されずにログインすることができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="815"/>
        <source>Obviously, this should only be done in situations where the user account does not need to be secure, such as a public terminal.</source>
        <translation>もちろん、これは公共の端末などで、ユーザーアカウントの安全性を確保する必要がない場合にのみ行うべきものです。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="823"/>
        <source>Old Home Directory</source>
        <translation>古い Home ディレクトリ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="824"/>
        <source>A home directory already exists for the user name you have chosen. This screen allows you to choose what happens to this directory.</source>
        <translation>選択したユーザー名の home ディレクトリが既に存在しています。この画面で、そのディレクトリをどうするか選択することができます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="826"/>
        <source>Re-use it for this installation</source>
        <translation>このインストールで再利用します</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="827"/>
        <source>The old home directory will be used for this user account. This is a good choice when upgrading, and your files and settings will be readily available.</source>
        <translation>古い home ディレクトリ用にこのユーザーアカウントを使用します。これはアップグレードの際に良い選択であり、ファイルや設定がすぐ利用できるようになります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="829"/>
        <source>Rename it and create a new directory</source>
        <translation>それを変更して新しいディレクトリを作成</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="830"/>
        <source>A new home directory will be created for the user, but the old home directory will be renamed. Your files and settings will not be immediately visible in the new installation, but can be accessed using the renamed directory.</source>
        <translation>新しい home ディレクトリが作成され、古い home ディレクトリ名は変更されます。ファイルや設定は新しいインストールではすぐには表示されませんが、変更後のディレクトリ名を使用してアクセスできます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="832"/>
        <source>The old directory will have a number at the end of it, depending on how many times the directory has been renamed before.</source>
        <translation>古いディレクトリには、以前に行った名前変更の回数にしたがって、そのディレクトリの最後に数字が表示されます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="833"/>
        <source>Delete it and create a new directory</source>
        <translation>それを削除し新しいディレクトリを作成</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="834"/>
        <source>The old home directory will be deleted, and a new one will be created from scratch.</source>
        <translation>古い home ディレクトリを削除し、新しい home ディレクトリを全く最初から作成します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="835"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="836"/>
        <source>All files and settings will be deleted permanently if this option is selected. Your chances of recovering them are low.</source>
        <translation>このオプションを選択した場合、すべてのファイルと設定は永久に削除されます。復元できる可能性が低くなります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="852"/>
        <source>Installation in Progress</source>
        <translation>インストール進行中</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="853"/>
        <source>%1 is installing. For a fresh install, this will probably take 3-20 minutes, depending on the speed of your system and the size of any partitions you are reformatting.</source>
        <translation>%1 をインストール中です。新規インストールの場合、システムの速度や再フォーマットするパーティションのサイズによっては、およそ3分から20分位かかります。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="855"/>
        <source>If you click the Abort button, the installation will be stopped as soon as possible.</source>
        <translation>中止ボタンをクリックすると、すぐインストールを中止します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="857"/>
        <source>Change settings while you wait</source>
        <translation>待機している間に設定を変更</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="858"/>
        <source>While %1 is being installed, you can click on the &lt;b&gt;Next&lt;/b&gt; or &lt;b&gt;Back&lt;/b&gt; buttons to enter other information required for the installation.</source>
        <translation>%1  がインストールされている間、&lt;b&gt;次へ&lt;/b&gt; ボタンまたは &lt;b&gt;戻る&lt;/b&gt; ボタンをクリックして、インストールに必要なその他の情報を入力できます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="860"/>
        <source>Complete these steps at your own pace. The installer will wait for your input if necessary.</source>
        <translation>ご自分のペースでこれらのステップを完了させてください。インストーラは必要に応じてあなたの入力を待ちます。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="868"/>
        <source>&lt;p&gt;&lt;b&gt;Congratulations!&lt;/b&gt;&lt;br/&gt;You have completed the installation of %1&lt;/p&gt;&lt;p&gt;&lt;b&gt;Finding Applications&lt;/b&gt;&lt;br/&gt;There are hundreds of excellent applications installed with %1 The best way to learn about them is to browse through the Menu and try them. Many of the apps were developed specifically for the %1 project. These are shown in the main menus. &lt;p&gt;In addition %1 includes many standard Linux applications that are run only from the command line and therefore do not show up in the Menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;おめでとうございます！&lt;/b&gt;&lt;br/&gt;%1 のインストールが完了しました&lt;/p&gt;&lt;p&gt;&lt;b&gt;アプリケーションの検索&lt;/b&gt;&lt;br/&gt;%1 には沢山の素晴らしいアプリケーションが入っています。まずはメニュー中を探索して下さい。多くのアプリは特に %1 環境のために作成されています。これはメインメニューの中に表示されています。&lt;p&gt;更に %1 はコマンドラインだけから動作する、メニューに表示されない標準の Linux アプリケーションを含んでいます。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="878"/>
        <location filename="../minstall.cpp" line="1144"/>
        <source>&lt;p&gt;&lt;b&gt;Support %1&lt;/b&gt;&lt;br/&gt;%1 is supported by people like you. Some help others at the support forum - %2 - or translate help files into different languages, or make suggestions, write documentation, or help test new software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;%1 のサポート&lt;/b&gt;&lt;br/&gt;%1 はあなたのような皆さんに支援されています。サポートフォーラム %2  で他の人を助ける、使用している言語の翻訳作業を行う、ドキュメント作成を行う、新しいバージョンのテストを行う、といった支援が可能です。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="908"/>
        <source>Finish</source>
        <translation>完了</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="911"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="913"/>
        <source>Next</source>
        <translation>次へ</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="423"/>
        <source>Configuring sytem. Please wait.</source>
        <translation>システムを構成しています。お待ちください。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="430"/>
        <source>Configuration complete. Restarting system.</source>
        <translation>設定が完了しました。システムを再起動します。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1066"/>
        <source>The installation and configuration is incomplete.
Do you really want to stop now?</source>
        <translation>インストールと設定がまだ完了していません。
本当に処理を中止したいのですか？</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1130"/>
        <source>&lt;p&gt;&lt;b&gt;Getting Help&lt;/b&gt;&lt;br/&gt;Basic information about %1 is at %2.&lt;/p&gt;&lt;p&gt;There are volunteers to help you at the %3 forum, %4&lt;/p&gt;&lt;p&gt;If you ask for help, please remember to describe your problem and your computer in some detail. Usually statements like &apos;it didn&apos;t work&apos; are not helpful.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;困った時に&lt;/b&gt;&lt;br/&gt;%1 の基本的な情報は %2 で参照できます。&lt;/p&gt;&lt;p&gt;%3 フォーラム %4 で助けてくれる人がいるでしょう。&lt;/p&gt;&lt;p&gt;もし助けを求める場合、その問題のとコンピュータ情報の詳細を忘れず記述してください。通常、「動きません」とだけ書かれたような内容は役に立ちません。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1138"/>
        <source>&lt;p&gt;&lt;b&gt;Repairing Your Installation&lt;/b&gt;&lt;br/&gt;If %1 stops working from the hard drive, sometimes it&apos;s possible to fix the problem by booting from LiveDVD or LiveUSB and running one of the included utilities in %1 or by using one of the regular Linux tools to repair the system.&lt;/p&gt;&lt;p&gt;You can also use your %1 LiveDVD or LiveUSB to recover data from MS-Windows systems!&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;インストールの修復&lt;/b&gt;&lt;br/&gt;%1 はハードドライブから作業を停止した場合、LiveDVD や LiveUSB からブートして、 %1 ツールのユーティリティまたはシステムを修復するための通常の Linux ツールを使用して問題を解決することが可能です。&lt;/p&gt;&lt;p&gt;また、MS-Windows システムからデータを回復するために %1 LiveDVD や LiveUSB を使用することができます！&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1152"/>
        <source>&lt;p&gt;&lt;b&gt;Adjusting Your Sound Mixer&lt;/b&gt;&lt;br/&gt; %1 attempts to configure the sound mixer for you but sometimes it will be necessary for you to turn up volumes and unmute channels in the mixer in order to hear sound.&lt;/p&gt; &lt;p&gt;The mixer shortcut is located in the menu. Click on it to open the mixer. &lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;サウンドミキサーの調整&lt;/b&gt;&lt;br/&gt;%1 はサウンドミキサーを設定します。ときには音を聞くためボリュームを上げたり、ミキサーのミュート解除が必要になるかもしれません。&lt;/p&gt; &lt;p&gt;ミキサーのショートカットはメニューにあります。ミキサーを開くにはこれをクリックしてください。 &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1160"/>
        <source>&lt;p&gt;&lt;b&gt;Keep Your Copy of %1 up-to-date&lt;/b&gt;&lt;br/&gt;For more information and updates please visit&lt;/p&gt;&lt;p&gt; %2&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;%1 のコピーを最新にする&lt;/b&gt;&lt;br/&gt; さらなる情報やアップデートについては&lt;/p&gt; %2 &lt;p&gt;参照してください。</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1165"/>
        <source>&lt;p&gt;&lt;b&gt;Special Thanks&lt;/b&gt;&lt;br/&gt;Thanks to everyone who has chosen to support %1 with their time, money, suggestions, work, praise, ideas, promotion, and/or encouragement.&lt;/p&gt;&lt;p&gt;Without you there would be no %1.&lt;/p&gt;&lt;p&gt;%2 Dev Team&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;特別な感謝&lt;/b&gt;&lt;br/&gt;%1 をサポートするために、自分の時間やお金、提案、仕事、賞賛、アイデア、プロモーション、激励を提供していただいた全ての方々に感謝いたします。&lt;/p&gt;&lt;p&gt;あなたなしでは %1 はなかったでしょう。&lt;/p&gt;&lt;p&gt;%2 開発チーム&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MeInstall</name>
    <message>
        <location filename="../meinstall.ui" line="43"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="51"/>
        <source>Live Log</source>
        <translation>ライブログ</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="160"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="86"/>
        <source>Next</source>
        <translation>次へ</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="177"/>
        <source>Gathering Information, please stand by.</source>
        <translation>情報を集めているのでしばらくお待ちください</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="209"/>
        <source>Terms of Use</source>
        <translation>利用規約</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="297"/>
        <source>Change Keyboard Settings</source>
        <translation>キーボードの設定を変更</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="375"/>
        <source>Select type of installation</source>
        <translation>インストールのタイプを選択</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="387"/>
        <source>Regular install using the entire disk</source>
        <translation>ディスク全体を利用して通常のインストール</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="400"/>
        <source>Customize the disk layout</source>
        <translation>ディスクレイアウトをカスタマイズ</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="413"/>
        <source>Use disk:</source>
        <translation>使用ディスク:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="567"/>
        <source>Encrypt</source>
        <translation>暗号化</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="579"/>
        <location filename="../meinstall.ui" line="782"/>
        <source>Encryption password:</source>
        <translation>暗号化パスワード:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="596"/>
        <location filename="../meinstall.ui" line="799"/>
        <source>Confirm password:</source>
        <translation>パスワードの確認:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="447"/>
        <source>Root</source>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="241"/>
        <source>Keyboard Settings</source>
        <translation>キーボードの設定</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="256"/>
        <source>Model:</source>
        <translation>モデル名：</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="271"/>
        <source>Variant:</source>
        <translation>バリエーション：</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="309"/>
        <source>Layout:</source>
        <translation>レイアウト：</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="457"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="532"/>
        <location filename="../meinstall.ui" line="554"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="616"/>
        <location filename="../meinstall.ui" line="1057"/>
        <source>Enable hibernation support</source>
        <translation>ハイバネーションサポートの有効化</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="654"/>
        <source>Choose partitions</source>
        <translation>パーティションの選択</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="694"/>
        <source>Query the operating system and reload the layouts of all drives.</source>
        <translation>オペレーティングシステムを再確認、すべてのドライブのレイアウトをリロードします。</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="727"/>
        <source>Remove an existing entry from the layout. This only works with entries to a new layout.</source>
        <translation>レイアウトから既存のエントリーを削除します。これは、新しいレイアウトへのエントリーでのみ機能します。</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="716"/>
        <source>Add a new partition entry. This only works with a new layout.</source>
        <translation>新規パーティションエントリを追加します。これは新規レイアウトのときだけ機能します。</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="663"/>
        <source>Show Grid</source>
        <translation>グリッド表示</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="670"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="680"/>
        <source>Mark the selected drive to be cleared for a new layout.</source>
        <translation>新規レイアウトのために、選択したドライブにマークを付けます。</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="705"/>
        <source>Run the partition management application of this operating system.</source>
        <translation>このOSのパーティション管理アプリを実行してください。</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="776"/>
        <source>Encryption options</source>
        <translation>暗号化オプション</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="835"/>
        <source>Install GRUB for Linux and Windows</source>
        <translation>Linux および Windows 向けに GRUB をインストール</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="859"/>
        <source>Master Boot Record</source>
        <translation>MBR (マスターブートレコード)</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="865"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="868"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="890"/>
        <source>EFI System Partition</source>
        <translation>EFI システムパーティション</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="893"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="925"/>
        <source>Partition Boot Record</source>
        <translation>パーティションの起動レコード</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="928"/>
        <source>PBR</source>
        <translation>PBR</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="941"/>
        <source>System boot disk:</source>
        <translation>システム起動ディスク:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="954"/>
        <source>Location to install on:</source>
        <translation>インストール先:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="990"/>
        <source>Create a swap file</source>
        <translation>swap ファイルを作成する</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1007"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1027"/>
        <source>Size:</source>
        <translation>サイズ：</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1050"/>
        <source>Location:</source>
        <translation>場所:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1089"/>
        <source>Common Services to Enable</source>
        <translation>共通サービスを有効に</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1108"/>
        <source>Service</source>
        <translation>サービス</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1113"/>
        <source>Description</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1146"/>
        <source>Computer Network Names</source>
        <translation>コンピュータネットワークの名前</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1173"/>
        <source>Workgroup</source>
        <translation>ワークグループ</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1186"/>
        <source>Workgroup:</source>
        <translation>ワークグループ:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1199"/>
        <source>SaMBa Server for MS Networking</source>
        <translation>MSネットワーク用 SaMBaサーバー</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1215"/>
        <source>example.dom</source>
        <translation>example.dom</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1228"/>
        <source>Computer domain:</source>
        <translation>コンピュータのドメイン:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1254"/>
        <source>Computer name:</source>
        <translation>コンピュータ名:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1315"/>
        <source>Configure Clock</source>
        <translation>時刻の設定</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1356"/>
        <source>Format:</source>
        <translation>フォーマット:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1384"/>
        <source>Timezone:</source>
        <translation>時刻帯:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1429"/>
        <source>System clock uses local time</source>
        <translation>システムクロックに現地時刻を使用する</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1461"/>
        <source>Localization Defaults</source>
        <translation>既定の言語と地域</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1501"/>
        <source>Locale:</source>
        <translation>ロケール:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1523"/>
        <source>Service Settings (advanced)</source>
        <translation>サービス設定 (高度な設定)</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1541"/>
        <source>Adjust which services should run at startup</source>
        <translation>起動時に実行されるサービスの調整</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1544"/>
        <source>View</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1589"/>
        <source>Desktop modifications made in the live environment will be carried over to the installed OS</source>
        <translation>ライブ環境で作られたデスクトップの修正がインストールされている OS へ引き継がれます</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1592"/>
        <source>Save live desktop changes</source>
        <translation>ライブデスクトップの変更を保存</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1605"/>
        <source>Default User Account</source>
        <translation>既定のユーザーアカウント</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1617"/>
        <source>Default user login name:</source>
        <translation>既定のユーザーログイン名:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1649"/>
        <source>Default user password:</source>
        <translation>既定のユーザーパスワード:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1678"/>
        <source>Confirm user password:</source>
        <translation>ユーザパスワードの確認:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1636"/>
        <source>username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1710"/>
        <source>Root (administrator) Account</source>
        <translation>Root (管理者) アカウント</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1728"/>
        <source>Root password:</source>
        <translation>root パスワード:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1757"/>
        <source>Confirm root password:</source>
        <translation>root パスワードの確認:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1789"/>
        <source>Autologin</source>
        <translation>自動ログイン</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1825"/>
        <source>Existing Home Directory</source>
        <translation>現在の home ディレクトリ</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1834"/>
        <source>What would you like to do with the old directory?</source>
        <translation>古いディレクトリをどうしますか？</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1841"/>
        <source>Re-use it for this installation</source>
        <translation>今回のインストールで再利用</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1848"/>
        <source>Rename it and create a new directory</source>
        <translation>名前変更し新しいディレクトリを作成</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1855"/>
        <source>Delete it and create a new directory</source>
        <translation>削除し新しいディレクトリを作成</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1900"/>
        <source>Tips</source>
        <translation>Tips（使い方のコツ）</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1944"/>
        <source>Installation complete</source>
        <translation>インストール完了</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1950"/>
        <source>Automatically reboot the system when the installer is closed</source>
        <translation>インストーラを閉じると自動的にシステムを再起動します</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1969"/>
        <source>Reminders</source>
        <translation>リマインダー</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="72"/>
        <source>Back</source>
        <translation>戻る</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="79"/>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="122"/>
        <source>Installation in progress</source>
        <translation>インストール進行中</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="137"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="140"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
</context>
<context>
    <name>Oobe</name>
    <message>
        <location filename="../oobe.cpp" line="331"/>
        <source>Please enter a computer name.</source>
        <translation>コンピューター名を入力してください。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="335"/>
        <source>Sorry, your computer name contains invalid characters.
You'll have to select a different
name before proceeding.</source>
        <translation>申しわけありませんがコンピューター名に無効な文字列が含まれています。
先へ進むには別の名前を選んでください。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="340"/>
        <source>Please enter a domain name.</source>
        <translation>ドメイン名を入力してください。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="344"/>
        <source>Sorry, your computer domain contains invalid characters.
You'll have to select a different
name before proceeding.</source>
        <translation>申しわけありませんがコンピューターのドメインに無効な文字列が含まれています。
先へ進むには別の名前を選んでください。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="351"/>
        <source>Please enter a workgroup.</source>
        <translation>ワークグループを入力してください。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="491"/>
        <source>The user name cannot contain special characters or spaces.
Please choose another name before proceeding.</source>
        <translation>ユーザー名には特殊文字や空白を含むことができません。
先へ進むには別の名前を選んで下さい。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="502"/>
        <source>Sorry, that name is in use.
Please select a different name.</source>
        <translation>申しわけありませんが、この名前はすでに使われています。
別の名前を選んでください。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="511"/>
        <source>You did not provide a passphrase for %1.</source>
        <translation>%1 のパスフレーズを入力していません。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="512"/>
        <source>Are you sure you want to continue?</source>
        <translation>本当に作業を続けてもよろしいですか？</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="518"/>
        <source>You did not provide a password for the root account. Do you want to continue?</source>
        <translation>rootアカウントのパスワードを入力していません。作業を続けますか？</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="531"/>
        <source>Failed to set user account passwords.</source>
        <translation>ユーザーアカウントのパスワード設定に失敗しました。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="557"/>
        <source>Failed to save old home directory.</source>
        <translation>以前の home ディレクトリの保存に失敗しました。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="566"/>
        <source>Failed to delete old home directory.</source>
        <translation>以前の home ディレクトリの削除に失敗しました。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="587"/>
        <source>Sorry, failed to create user directory.</source>
        <translation>ユーザーディレクトリの生成に失敗しました。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="590"/>
        <source>Sorry, failed to name user directory.</source>
        <translation>ユーザーディレクトリの命名に失敗しました。</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="626"/>
        <source>Failed to set ownership or permissions of user directory.</source>
        <translation>ユーザーディレクトリの権限またはパーミッションの設定に失敗しました。</translation>
    </message>
</context>
<context>
    <name>PartMan</name>
    <message>
        <location filename="../partman.cpp" line="222"/>
        <source>Virtual Devices</source>
        <translation>仮想デバイス</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="451"/>
        <location filename="../partman.cpp" line="511"/>
        <source>&amp;Add partition</source>
        <translation>パーティション追加(&amp;A)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="453"/>
        <source>&amp;Remove partition</source>
        <translation>パーティション削除(&amp;R)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="463"/>
        <source>&amp;Lock</source>
        <translation>ロック(&amp;L)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="467"/>
        <source>&amp;Unlock</source>
        <translation>ロック解除(&amp;U)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="471"/>
        <location filename="../partman.cpp" line="623"/>
        <source>Add to crypttab</source>
        <translation>crypttab へ追加</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="476"/>
        <source>Active partition</source>
        <translation>アクティブパーティション</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="477"/>
        <source>EFI System Partition</source>
        <translation>EFI システムパーティション</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="485"/>
        <source>New subvolume</source>
        <translation>新規サブボリューム</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="486"/>
        <source>Scan subvolumes</source>
        <translation>サブボリュームのスキャン</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="514"/>
        <source>New &amp;layout</source>
        <translation>新規レイアウト(_&amp;)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="515"/>
        <source>&amp;Reset layout</source>
        <translation>レイアウトのリセット(&amp;R)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="538"/>
        <source>Default subvolume</source>
        <translation>デフォルトのサブボリューム</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="539"/>
        <source>Remove subvolume</source>
        <translation>サブボリュームの削除</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="621"/>
        <source>Unlock Drive</source>
        <translation>ドライブのロック解除</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="626"/>
        <source>Password:</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="652"/>
        <source>Could not unlock device. Possible incorrect password.</source>
        <translation>デバイスをロック解除できません。パスワードが正しくないようです。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="680"/>
        <source>Failed to close %1</source>
        <translation>%1 の近くで失敗しました</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="724"/>
        <source>Invalid subvolume label</source>
        <translation>無効なサブボリュームラベル</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="733"/>
        <source>Duplicate subvolume label</source>
        <translation>重複したサブボリュームラベル</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="742"/>
        <source>Invalid use for %1: %2</source>
        <translation>無効な %1: %2 の利用</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="753"/>
        <source>%1 is already selected for: %2</source>
        <translation>%1 はすでに %2 で選択されています。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="767"/>
        <source>A root partition of at least %1 is required.</source>
        <translation>少なくとも %1の root パーティションが必要です。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="773"/>
        <source>Cannot preserve /home inside root (/) if a separate /home partition is also mounted.</source>
        <translation>別の独立した /home パーティションがマウントされている場合、root (/) 内に /home を保持することはできません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="789"/>
        <source>Reuse (no reformat) %1</source>
        <translation>%1 を再使用 (フォーマットしません)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="792"/>
        <source>Format %1</source>
        <translation>%1 のフォーマット</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="808"/>
        <source>Reuse subvolume %1 as %2</source>
        <translation>サブボリューム %1 を %2 として再使用する</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="810"/>
        <source>Delete subvolume %1</source>
        <translation>サブボリューム %1 を削除</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="813"/>
        <source>Overwrite subvolume %1</source>
        <translation>サブボリューム %1 を上書き</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="814"/>
        <source>Overwrite subvolume %1 to use for %2</source>
        <translation>サブボリューム %1 を上書きし、 %2 に利用する</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="816"/>
        <source>Create subvolume %1</source>
        <translation>サブボリューム %1 を作成する</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="817"/>
        <source>Create subvolume %1 to use for %2</source>
        <translation>サブボリューム %1 を作成し、%2 に利用する</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="832"/>
        <source>You must choose a separate boot partition when encrypting root.</source>
        <translation>暗号化するには別の独立した boot パーティションを選択する必要があります。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="783"/>
        <source>Prepare %1 partition table on %2</source>
        <translation>%2 で %1 パーティションテーブルを準備します</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="793"/>
        <source>Format %1 to use for %2</source>
        <translation>%2 に使用する %1 を初期化します</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="794"/>
        <source>Reuse (no reformat) %1 as %2</source>
        <translation>%1 を %2 として再利用（再フォーマット不要）</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="795"/>
        <source>Delete the data on %1 except for /home, to use for %2</source>
        <translation>/home 以外の %1 のデータを削除して %2 に使用します</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="798"/>
        <source>Create %1 without formatting</source>
        <translation>初期化しないで %1 を作成します</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="799"/>
        <source>Create %1, format to use for %2</source>
        <translation>%1 を作成し、%2 に使用するため初期化します</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="948"/>
        <source>The following drives are, or will be, setup with GPT, but do not have a BIOS-GRUB partition:</source>
        <translation>以下のドライブは現在 GPT で設定されているか、または設定される予定ですが、BIOS-GRUB パーティションが存在しません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="950"/>
        <source>This system may not boot from GPT drives without a BIOS-GRUB partition.</source>
        <translation>このシステムでは、BIOS-GRUB パーティションが存在しないので、GPT ドライブから起動しないかもしれません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="833"/>
        <location filename="../partman.cpp" line="907"/>
        <location filename="../partman.cpp" line="925"/>
        <location filename="../partman.cpp" line="951"/>
        <source>Are you sure you want to continue?</source>
        <translation>本当に作業を続けてもよろしいですか？</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="517"/>
        <source>Layout &amp;Builder...</source>
        <translation>Layout &amp;Builder...</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="879"/>
        <source>%1 (%2) requires %3</source>
        <translation>%1 (%2) には %3 が必要です</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="905"/>
        <source>The installation may fail because the following volumes are too small:</source>
        <translation>以下のボリュームが小さすぎるので、インストールに失敗する可能性があります。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="839"/>
        <source>The %1 installer will now perform the requested actions.</source>
        <translation>%1 インストーラは今から指示された動作を実行します。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="840"/>
        <source>These actions cannot be undone. Do you want to continue?</source>
        <translation>これから先の操作は元に戻すことができません。続けますか？</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="918"/>
        <source>This system uses EFI, but no valid EFI system partition was assigned to /boot/efi separately.</source>
        <translation>このシステムでは EFI を使用していますが、/boot/efi に有効な EFI システムパーティションが割り当てられていません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="921"/>
        <source>The volume assigned to /boot/efi is not a valid EFI system partition.</source>
        <translation>boot/efi に割り当てられたボリュームは、有効な EFI システムパーティションではありません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="988"/>
        <source>The disks with the partitions you selected for installation are failing:</source>
        <translation>インストール用に選択したディスクのパーディションに障害が発生しています：</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="992"/>
        <source>Smartmon tool output:</source>
        <translation>Smartmonツールの出力：</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="993"/>
        <source>The disks with the partitions you selected for installation pass the SMART monitor test (smartctl), but the tests indicate it will have a higher than average failure rate in the near future.</source>
        <translation>インストール用に選択したディスクのパーディションは SMART モニターテスト (smartctl)に合格しましたが、テストは近い将来、平均よりも高い故障率になることを示しています。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="998"/>
        <source>If unsure, please exit the Installer and run GSmartControl for more information.</source>
        <translation>よく分からないのであれば、このインストーラを終了し、GSmartControl を起動して詳細をつかんでください。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1000"/>
        <source>Do you want to abort the installation?</source>
        <translation>インストールを中止してよろしいですか？</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1005"/>
        <source>Do you want to continue?</source>
        <translation>作業を続けてもよろしいですか？</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1211"/>
        <source>Failed to format LUKS container.</source>
        <translation> LUKS コンテナの初期化に失敗しました。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1231"/>
        <source>Failed to open LUKS container.</source>
        <translation>LUKS コンテナを開くことができません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1092"/>
        <source>Failed to prepare required partitions.</source>
        <translation>必要なパーティションを準備できません。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1131"/>
        <source>Preparing partition tables</source>
        <translation>パーティションテーブルを準備しています</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1148"/>
        <source>Preparing required partitions</source>
        <translation>必要なパーティションを準備中</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1216"/>
        <source>Creating encrypted volume: %1</source>
        <translation>暗号化ボリュームの作成中: %1</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1251"/>
        <source>Formatting: %1</source>
        <translation>フォーマット中: %1</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1244"/>
        <source>Failed to format partition.</source>
        <translation>パーティションの初期化に失敗しました。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1309"/>
        <source>Failed to prepare subvolumes.</source>
        <translation>サブボリュームの準備に失敗しました。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1310"/>
        <source>Preparing subvolumes</source>
        <translation>サブボリュームを準備中</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1408"/>
        <source>Failed to mount partition.</source>
        <translation>パーティションのマウントに失敗しました。</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1413"/>
        <source>Mounting: %1</source>
        <translation>マウント中: %1</translation>
    </message>
</context>
<context>
    <name>PartMan::ItemDelegate</name>
    <message>
        <location filename="../partman.cpp" line="2457"/>
        <source>&amp;Templates</source>
        <translation>テンプレート(&amp;T)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="2465"/>
        <source>Compression (Z&amp;STD)</source>
        <translation>圧縮 (Z&amp;STD)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="2467"/>
        <source>Compression (&amp;LZO)</source>
        <translation>圧縮 (&amp;LZO)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="2469"/>
        <source>Compression (&amp;ZLIB)</source>
        <translation>圧縮 (&amp;ZLIB)</translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="../passedit.cpp" line="144"/>
        <source>Negligible</source>
        <translation>わずかな</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="144"/>
        <source>Very weak</source>
        <translation>非常に弱い</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="144"/>
        <source>Weak</source>
        <translation>弱い</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="145"/>
        <source>Moderate</source>
        <translation>ほど良い</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="145"/>
        <source>Strong</source>
        <translation>強い</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="145"/>
        <source>Very strong</source>
        <translation>非常に強い</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="147"/>
        <source>Password strength: %1</source>
        <translation>パスワードの強度: %1</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="180"/>
        <source>Hide the password</source>
        <translation>パスワードを隠す</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="180"/>
        <source>Show the password</source>
        <translation>パスワードを表示する</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../app.cpp" line="89"/>
        <source>Customizable GUI installer for MX Linux and antiX Linux</source>
        <translation>MX Linux および antiX Linux用のカスタマイズ可能なGUIインストーラ</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="92"/>
        <source>Installs automatically using the configuration file (more information below).
-- WARNING: potentially dangerous option, it will wipe the partition(s) automatically.</source>
        <translation>設定ファイルを使って自動的にインストールします（詳細は後述にあります）。
-- 警告: 潜在的に危険なオプションです。これはパーティションを自動的に消去します。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="94"/>
        <source>Overrules sanity checks on partitions and drives, causing them to be displayed.
-- WARNING: this can break things, use it only if you don&apos;t care about data on drive.</source>
        <translation>パーティションやドライブのサニティーチェックを無効にして表示させます。
-- 警告：これはデータを壊す可能性があるので、ドライブ上のデータを気にしない場合にのみ使用してください。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="96"/>
        <source>Load a configuration file as specified by &lt;config-file&gt;.
By default /etc/minstall.conf is used.
This configuration can be used with --auto for an unattended installation.
The installer creates (or overwrites) /mnt/antiX/etc/minstall.conf and saves a copy to /etc/minstalled.conf for future use.
The installer will not write any passwords or ignored settings to the new configuration file.
Please note, this is experimental. Future installer versions may break compatibility with existing configuration files.</source>
        <translation>&lt;config-file&gt; で指定された設定ファイルを読み込みます。
デフォルトでは /etc/minstall.conf を使用します。
この設定は、--auto と併用することで、自動インストールが可能です。
インストーラは、パスワードや無視された設定を新しい設定ファイルに書き込みません。インストーラは、/mnt/antiX/etc/minstall.conf を作成（または上書き）し、将来使用するために /etc/minstalled.conf にコピーを保存します。
インストーラは、パスワードや無視された設定を新しい設定ファイルには書き込みません。
注記：これは実験的なものです。将来公開されるインストーラでは、既存の設定ファイルとの互換性が失われる可能性があります。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="102"/>
        <source>Shutdown automatically when done installing.</source>
        <translation>インストールが終わると、自動的にシャットダウンします。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="103"/>
        <source>Always use GPT when doing a whole-drive installation regardlesss of capacity.
Without this option, GPT will only be used on drives with at least 2TB capacity.
GPT is always used on whole-drive installations on UEFI systems regardless of capacity, even without this option.</source>
        <translation>ドライブ全体のインストールを行う場合、容量に関わらず常にGPTを使用してください。
このオプションがない場合、GPTは2TB以上の容量のドライブでのみ使用されます。
このオプションがない場合でさえ、UEFI システム上のドライブ全体のインストールでは、容量に関わらず常に GPT が使用されます。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="106"/>
        <source>Do not unmount /mnt/antiX or close any of the associated LUKS containers when finished.</source>
        <translation>完了時には、/mnt/antiX をアンマウントしたり、関連する LUKS コンテナを閉じたりしないでください。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="107"/>
        <source>Another testing mode for installer, partitions/drives are going to be FORMATED, it will skip copying the files.</source>
        <translation>インストーラの別のテストモードでは、パーティション/ドライブがフォーマットされ、ファイルのコピーがスキップされます。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="108"/>
        <source>Install the operating system, delaying prompts for user-specific options until the first reboot.
Upon rebooting, the installer will be run with --oobe so that the user can provide these details.
This is useful for OEM installations, selling or giving away a computer with an OS pre-loaded on it.</source>
        <translation>オペレーティングシステムをインストールし、ユーザー特有のオプションの表示を最初の再起動まで遅延します。
再起動時に --oobe を付けてインストーラが実行されるので、ユーザーにはこうした情報が提供されます
これは、OEMインストールや、OSがプリインストールされたコンピュータを販売したり、譲ったりする際に便利です。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="111"/>
        <source>Out Of the Box Experience option.
This will start automatically if installed with --oem option.</source>
        <translation>Out Of the Box 体験オプション。
--oem オプションでインストールした場合、自動的に起動します。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="113"/>
        <source>Test mode for GUI, you can advance to different screens without actially installing.</source>
        <translation>GUIのテストモードでは、実際にインストールすることなく、さまざまな画面に進むことができます。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="114"/>
        <source>Reboots automatically when done installing.</source>
        <translation>インストールが完了すると自動的に再起動します。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="115"/>
        <source>Installing with rsync instead of cp on custom partitioning.
-- doesn&apos;t format /root and it doesn&apos;t work with encryption.</source>
        <translation>カスタムパーティショニングで、cpの代わりにrsyncでインストールします。
-- /rootをフォーマットせず、暗号化にも対応しません。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="117"/>
        <source>Always check the installation media at the beginning.</source>
        <translation>インストールメディアは、必ず最初にチェック（確認）する。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="118"/>
        <source>Do not check the installation media at the beginning.
Not recommended unless the installation media is guaranteed to be free from errors.</source>
        <translation>インストールメディアは最初の時点では確認しない。

この方法は、インストールメディアにエラーがないことが保証されている場合を除き、推奨しません。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="122"/>
        <source>Load a configuration file as specified by &lt;config-file&gt;.</source>
        <translation>&lt;config-file&gt; で指定された設定ファイルを読み込みます。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="126"/>
        <source>Too many arguments. Please check the command format by running the program with --help</source>
        <translation>引数が多すぎます。--help でプログラムを実行してコマンドの形式を確認してください。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="131"/>
        <source>%1 Installer</source>
        <translation>%1 インストーラ</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="139"/>
        <source>The installer won't launch because it appears to be running already in the background.

Please close it if possible, or run &apos;pkill minstall&apos; in terminal.</source>
        <translation>インストーラは、既にバックグラウンドで実行されているようなので起動しません。

可能であれば閉じてください。または端末で &apos;pkill minstall&apos; を実行して下さい。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="146"/>
        <source>This operation requires root access.</source>
        <translation>この操作には root 権限が必要です。</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="167"/>
        <source>Configuration file (%1) not found.</source>
        <translation>設定ファイル (%1) が見つかりません。</translation>
    </message>
</context>
<context>
    <name>SwapMan</name>
    <message>
        <location filename="../swapman.cpp" line="85"/>
        <source>Failed to create or install swap file.</source>
        <translation>スワップファイルの作成、またはインストールに失敗しました。</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="91"/>
        <source>Creating swap file</source>
        <translation>スワップファイルを作成しています</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="103"/>
        <source>Configuring swap file</source>
        <translation>スワップファイルを設定しています</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="67"/>
        <source>Invalid location</source>
        <translation>無効な場所です</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="70"/>
        <source>Maximum: %1 MB</source>
        <translation>最大限： %1 MB</translation>
    </message>
</context>
</TS>