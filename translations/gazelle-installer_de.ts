<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="de">
<context>
    <name>AutoPart</name>
    <message>
        <location filename="../autopart.cpp" line="58"/>
        <source>Root</source>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="59"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="140"/>
        <location filename="../autopart.cpp" line="142"/>
        <source>Recommended: %1
Minimum: %2</source>
        <translation>Empfohlen: %1
Minimum: %2</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="179"/>
        <source>Layout Builder</source>
        <translation>Layout-Erstellung</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="323"/>
        <source>%1% root
%2% home</source>
        <translation>%1% root
%2% home</translation>
    </message>
    <message>
        <location filename="../autopart.cpp" line="325"/>
        <source>Combined root and home</source>
        <translation>Kombinierte »root« und »home« Partition</translation>
    </message>
</context>
<context>
    <name>Base</name>
    <message>
        <location filename="../base.cpp" line="56"/>
        <source>Cannot access installation media.</source>
        <translation>Zugriff auf Installationsmedium nicht möglich.</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="156"/>
        <source>Deleting old system</source>
        <translation>Das alte System löschen</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="166"/>
        <source>Failed to set the system configuration.</source>
        <translation>Die Systemkonfiguration konnte nicht eingestellt werden.</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="168"/>
        <source>Setting system configuration</source>
        <translation>Einstellung der Systemkonfiguration</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="153"/>
        <source>Failed to delete old system on destination.</source>
        <translation>Löschen des alten Systems auf dem Zielort fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="246"/>
        <source>Copying new system</source>
        <translation>Das neue System kopieren</translation>
    </message>
    <message>
        <location filename="../base.cpp" line="276"/>
        <source>Failed to copy the new system.</source>
        <translation>Kopieren des neuen Systems fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>BootMan</name>
    <message>
        <location filename="../bootman.cpp" line="277"/>
        <source>Updating initramfs</source>
        <translation>initramfs wird aktualisiert</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="128"/>
        <source>Installing GRUB</source>
        <translation>GRUB installieren</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="106"/>
        <source>GRUB installation failed. You can reboot to the live medium and use the GRUB Rescue menu to repair the installation.</source>
        <translation>Die GRUB Installation ist fehlgeschlagen. Sie können mit dem Live-Medium neu booten und über das GRUB-Rettungsmenü die Installation reparieren.</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="278"/>
        <source>Failed to update initramfs.</source>
        <translation>Aktualisierung von initramfs fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="303"/>
        <source>System boot disk:</source>
        <translation>Systemstart-Laufwerk:</translation>
    </message>
    <message>
        <location filename="../bootman.cpp" line="322"/>
        <location filename="../bootman.cpp" line="332"/>
        <source>Partition to use:</source>
        <translation>genutzte Partition:</translation>
    </message>
</context>
<context>
    <name>CheckMD5</name>
    <message>
        <location filename="../checkmd5.cpp" line="38"/>
        <source>Checking installation media.</source>
        <translation>Das Installationsmedium wird geprüft.</translation>
    </message>
    <message>
        <location filename="../checkmd5.cpp" line="39"/>
        <source>Press ESC to skip.</source>
        <translation>Zum Überspringen bitte ESC drücken.</translation>
    </message>
    <message>
        <location filename="../checkmd5.cpp" line="56"/>
        <source>The installation media is corrupt.</source>
        <translation>Das Installationsmedium ist beschädigt.</translation>
    </message>
    <message>
        <location filename="../checkmd5.cpp" line="120"/>
        <source>Are you sure you want to skip checking the installation media?</source>
        <translation>Soll die Prüfung des Installationsmedium wirklich übersprungen werden?</translation>
    </message>
</context>
<context>
    <name>MInstall</name>
    <message>
        <location filename="../minstall.cpp" line="87"/>
        <source>Shutdown</source>
        <translation>Herunterfahren</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="142"/>
        <source>You are running 32bit OS started in 64 bit UEFI mode, the system will not be able to boot unless you select Legacy Boot or similar at restart.
We recommend you quit now and restart in Legacy Boot

Do you want to continue the installation?</source>
        <translation>Sie verwenden ein 32bit Betriebssystem, das im 64bit UEFI-Modus gestartet wurde. Das System wird nicht booten können, außer sie wählen Legacy Boot oder ähnliches beim Neustart.
Wir empfehlen jetzt zu beenden und im Legacy Boot Modus neu zu starten

Mit der Installation fortfahren?</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="186"/>
        <source>Support %1

%1 is supported by people like you. Some help others at the support forum - %2, or translate help files into different languages, or make suggestions, write documentation, or help test new software.</source>
        <translation>Unterstützung für %1

%1 wird von Leuten wie ihnen unterstützt. Einige helfen anderen Nutzern im Forum - %2 oder übersetzen Dateien in andere Sprachen oder machen Verbesserungsvorschläge, schreiben Dokumentationen oder helfen beim Testen neuer Programme.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="226"/>
        <source>%1 is an independent Linux distribution based on Debian Stable.

%1 uses some components from MEPIS Linux which are released under an Apache free license. Some MEPIS components have been modified for %1.

Enjoy using %1</source>
        <translation>%1 ist eine unabhängige Linux-Distribution basierend auf Debian Stable.

%1 nutzt einige Komponenten von MEPIS Linux, die unter einer freien Apache-Lizenz stehen. Einige MEPIS- Komponenten wurden für %1 modifiziert.

Viel Spaß mit %1</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="344"/>
        <source>Pretending to install %1</source>
        <translation>Simulieren, %1 zu installieren</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="362"/>
        <source>Preparing to install %1</source>
        <translation>Vorbereitung der Installation von %1</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="383"/>
        <source>Paused for required operator input</source>
        <translation>Pausiert für erforderliche Bedienereingaben</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="395"/>
        <source>Setting system configuration</source>
        <translation>Einstellung der Systemkonfiguration</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="407"/>
        <source>Cleaning up</source>
        <translation>Bereinigung läuft</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="411"/>
        <source>Finished</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="435"/>
        <source>The installation was aborted.</source>
        <translation>Die Installation wurde abgebrochen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="521"/>
        <source>Invalid settings found in configuration file (%1). Please review marked fields as you encounter them.</source>
        <translation>Ungültige Einstellungen in der Konfigurationsdatei (%1) gefunden. Bitte überprüfen sie die markierten Felder, wenn sie sie finden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="541"/>
        <source>OK to format and use the entire disk (%1) for %2?</source>
        <translation>Einverstanden mit der Formatierung der Festplatte (%1) und Benutzung der ganzen Platte durch %2?</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="545"/>
        <source>WARNING: The selected drive has a capacity of at least 2TB and must be formatted using GPT. On some systems, a GPT-formatted disk will not boot.</source>
        <translation>WARNUNG: Das ausgewählte Laufwerk hat eine Kapazität von mindestens 2 TB und muss mit GPT formatiert sein. Auf einigen Systemen lässt sich eine GPT-formatierte Festplatte nicht booten.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="574"/>
        <source>The data in /home cannot be preserved because the required information could not be obtained.</source>
        <translation>Die Daten in /home können nicht erhalten werden, da die erforderlichen Informationen nicht abgerufen werden konnten.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="601"/>
        <source>The home directory for %1 already exists.</source>
        <translation>Das Home-Verzeichnis für %1 existiert bereits.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="637"/>
        <source>General Instructions</source>
        <translation>Allgemeine Anleitungen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="638"/>
        <source>BEFORE PROCEEDING, CLOSE ALL OTHER APPLICATIONS.</source>
        <translation>Vor dem Fortfahren ALLE ANDEREN ANWENDUNGEN SCHLIESSEN.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="639"/>
        <source>On each page, please read the instructions, make your selections, and then click on Next when you are ready to proceed. You will be prompted for confirmation before any destructive actions are performed.</source>
        <translation>Auf jeder Seite bitte die Anleitungen lesen, die Auswahlen treffen und dann auf NÄCHSTES klicken sobald alles bereit zum Fortfahren ist. Bevor destruktive Aktionen durchgeführt werden erscheint grundsätzliche eine Bitte um Bestätigung .</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="641"/>
        <source>Limitations</source>
        <translation>Einschränkungen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="642"/>
        <source>Remember, this software is provided AS-IS with no warranty what-so-ever. It is solely your responsibility to backup your data before proceeding.</source>
        <translation>Es soll hier daran erinnert werden, daß diese Software ohne jegliche Garantie zur Verfügung gestellt wird. Sie sind selbst dafür verantwortlich, eine Sicherung ihrer Daten vorzunehmen, bevor sie fortfahren.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="647"/>
        <source>Installation Options</source>
        <translation>Installationsoptionen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="648"/>
        <source>If you are running Mac OS or Windows OS (from Vista onwards), you may have to use that system&apos;s software to set up partitions and boot manager before installing.</source>
        <translation>Sollten sie Mac OS oder Windows OS (ab Vista) benutzen, müssen sie möglicherweise vor der Installation deren Systemprogramme zum Einrichten der Partitionen und des Bootloaders verwenden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="649"/>
        <source>Using the root-home space slider</source>
        <translation>Verwenden sie bitte den Schieberegeler für die Größenanpassung des root-home-Bereiches.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="650"/>
        <source>The drive can be divided into separate system (root) and user data (home) partitions using the slider.</source>
        <translation>Das Laufwerk kann mit dem Schieberegler in separate System- (root) und Benutzerdaten- (home) Partitionen unterteilt werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="651"/>
        <source>The &lt;b&gt;root&lt;/b&gt; partition will contain the operating system and applications.</source>
        <translation>Die &lt;b&gt;root&lt;/b&gt; Partition wird das Betriebssystem und die Programme aufnehmen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="652"/>
        <source>The &lt;b&gt;home&lt;/b&gt; partition will contain the data of all users, such as their settings, files, documents, pictures, music, videos, etc.</source>
        <translation>Auf der &lt;b&gt;home&lt;/b&gt; Partition werden die Daten aller Anwender gespeichert, wie z.B. persönliche Einstellungen, Dateien, Dokumente, Bilder, Musik Filme etc.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="653"/>
        <source>Move the slider to the right to increase the space for &lt;b&gt;root&lt;/b&gt;. Move it to the left to increase the space for &lt;b&gt;home&lt;/b&gt;.</source>
        <translation>Bewegen sie den Schieberegler nach rechts, um den Platz für &lt;b&gt;root&lt;/b&gt; zu vergrößern. Schieben sie ihn nach links, um den Platz für &lt;b&gt;home&lt;/b&gt;zu vergrößern. </translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="654"/>
        <source>Move the slider all the way to the right if you want both root and home on the same partition.</source>
        <translation>Schieben sie den Schieberegler ganz nach rechts, wenn sie sowohl root als auch home auf der gleichen Partition haben möchten.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="655"/>
        <source>Keeping the home directory in a separate partition improves the reliability of operating system upgrades. It also makes backing up and recovery easier. This can also improve overall performance by constraining the system files to a defined portion of the drive.</source>
        <translation>Die Aufbewahrung des Home-Verzeichnisses auf einer separaten Partition verbessert die Zuverlässigkeit von Betriebssystem-Upgrades. Außerdem wird dadurch die Sicherung und Wiederherstellung einfacher. Dies kann auch die Gesamtleistung verbessern, indem die Systemdateien auf einen definierten Teil des Laufwerks beschränkt werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="657"/>
        <location filename="../minstall.cpp" line="741"/>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="658"/>
        <location filename="../minstall.cpp" line="742"/>
        <source>Encryption is possible via LUKS. A password is required.</source>
        <translation>Die Verschlüsselung wird mit Hilfe von LUKS eingerichtet. Ein Passwort ist erforderlich.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="659"/>
        <location filename="../minstall.cpp" line="743"/>
        <source>A separate unencrypted boot partition is required.</source>
        <translation>Eine separate unverschlüsselte boot-Partition ist erforderlich.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="660"/>
        <source>When encryption is used with autoinstall, the separate boot partition will be automatically created.</source>
        <translation>Bei der Autoinstallation mit Verschlüsselung wird eine eigene Boot-Partition automatisch erzeugt.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="661"/>
        <source>Using a custom disk layout</source>
        <translation>Verwenden einer benutzerdefinierten Festplatteneinteilung.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="662"/>
        <source>If you need more control over where %1 is installed to, select &quot;&lt;b&gt;%2&lt;/b&gt;&quot; and click &lt;b&gt;Next&lt;/b&gt;. On the next page, you will then be able to select and configure the storage devices and partitions you need.</source>
        <translation>Falls Sie mehr Einfluß darüber haben wollen, an welchem Speicherort %1 installiert wird, wählen sie &quot;&lt;b&gt;%2&lt;/b&gt;&quot; und klicken auf &lt;b&gt;Weiter&lt;/b&gt;. Sie erhalten damit Zugriff auf die Auswahl und Konfiguration der Speichergeräte und Partitionen Ihrer Wahl.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="669"/>
        <source>Choose Partitions</source>
        <translation>Partitionen auswählen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="670"/>
        <source>The partition list allows you to choose what partitions are used for this installation.</source>
        <translation>In der Partitionsliste können sie auswählen, welche Partitionen für diese Installation verwendet werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="671"/>
        <source>&lt;i&gt;Device&lt;/i&gt; - This is the block device name that is, or will be, assigned to the created partition.</source>
        <translation>&lt;i&gt;Gerät&lt;/i&gt; - Name des blockorientierten Gerätes, der der erzeugten Partition zugewiesen wird.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="672"/>
        <source>&lt;i&gt;Size&lt;/i&gt; - The size of the partition. This can only be changed on a new layout.</source>
        <translation>&lt;i&gt;Größe&lt;/i&gt;- Die Größe der Partition. Dies kann nur bei einer neuen Eeinteilung geändert werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="673"/>
        <source>&lt;i&gt;Use For&lt;/i&gt; - To use this partition in an installation, you must select something here.</source>
        <translation>&lt;i&gt;Verwenden als&lt;/i&gt; — Um diese Partition für die Installation zu verwenden, hier eine Auswahl treffen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="688"/>
        <source>In addition to the above, you can also type your own mount point. Custom mount points must start with a slash (&quot;/&quot;).</source>
        <translation>Zusätzlich zu den oben genannten Optionen können eigene Einhängepunkte angegeben werden. Diese benutzerdefinierten Einhängepunkte müssen stets mit einem Schrägstrich beginnen (»/«).</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="689"/>
        <source>&lt;i&gt;Label&lt;/i&gt; - The label that is assigned to the partition once it has been formatted.</source>
        <translation>&lt;i&gt;Label&lt;/i&gt; - Das Label das der Partition zugewiesen wird, nachdem sie formatiert worden ist.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="690"/>
        <source>&lt;i&gt;Encrypt&lt;/i&gt; - Use LUKS encryption for this partition. The password applies to all partitions selected for encryption.</source>
        <translation>&lt;i&gt;Verschlüsseln&lt;/i&gt; —  LUKS Verschlüsselung für diese Partition verwenden. Das Paßwort gilt für alle Partitionen, für welche die Option zur Verschlüsselung ausgewählt wurde.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="691"/>
        <source>&lt;i&gt;Format&lt;/i&gt; - This is the partition&apos;s format. Available formats depend on what the partition is used for. When working with an existing layout, you may be able to preserve the format of the partition by selecting &lt;b&gt;Preserve&lt;/b&gt;.</source>
        <translation>&lt;i&gt;Format&lt;/i&gt; - Auswahl des Dateisystemtyps für die Partition. Welche Typen für die Formatierung zur Auswahl stehen, ist davon abhängig, wofür die Partition verwendet werden soll. Bei vorgefundenen Partitionen kann das bestehende Dateisystem durch Auswahl von &lt;b&gt;Beibehalten&lt;/b&gt;ohne Neuformatierung übernommen werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="695"/>
        <source>The ext2, ext3, ext4, jfs, xfs and btrfs Linux filesystems are supported and ext4 is recommended.</source>
        <translation>Die ext2-, ext3-, ext4-, jfs-, xfs- and btrfs-Linux-Dateisysteme werden unterstützt und ext4 wird empfohlen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="696"/>
        <source>&lt;i&gt;Check&lt;/i&gt; - Check and correct for bad blocks on the drive (not supported for all formats). This is very time consuming, so you may want to skip this step unless you suspect that your drive has bad blocks.</source>
        <translation>&lt;i&gt;Prüfen&lt;/i&gt; - Überprüfen und korrigieren des Laufwerks auf fehlerhafte Blöcke (wird nicht von allen Formaten unterstützt). Dies ist sehr zeitaufwendig, daher sollten sie diesen Schritt überspringen, es sei denn, sie vermuten, dass Ihr Laufwerk fehlerhafte Blöcke enthält.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="698"/>
        <source>&lt;i&gt;Mount Options&lt;/i&gt; - This specifies mounting options that will be used for this partition.</source>
        <translation>&lt;i&gt;Einhängeoptionen&lt;/i&gt; - Auswahl der Optionen zum Einhängen der gewählten Partition.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="699"/>
        <source>&lt;i&gt;Dump&lt;/i&gt; - Instructs the dump utility to include this partition in the backup.</source>
        <translation>&lt;i&gt;Sichern&lt;/i&gt; — Weist das Backup-Programm an, diese Partition in die Sicherung einzubeziehen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="700"/>
        <source>&lt;i&gt;Pass&lt;/i&gt; - The sequence in which this file system is to be checked at boot. If zero, the file system is not checked.</source>
        <translation>&lt;i&gt;Laufwerksprüfung&lt;/i&gt; - Legt die Reihenfolge fest, in der dieses Dateisystem beim Bootprozess überprüft werden wird. Eine 0 (Null) schließt dieses Dateisystem von der Prüfung aus.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="701"/>
        <source>Menus and actions</source>
        <translation>Menüs und Makros</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="702"/>
        <source>A variety of actions are available by right-clicking any drive or partition item in the list.</source>
        <translation>Durch Rechtsklick auf einen Laufwerks- oder Partitionseintrag stehen vielfältige Auswahloptionen zur Verfügung.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="703"/>
        <source>The buttons to the right of the list can also be used to manipulate the entries.</source>
        <translation>Auch die Schaltflächen rechts der Liste dienen der Anpassung der Einträge. </translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="704"/>
        <source>The installer cannot modify the layout already on the drive. To create a custom layout, mark the drive for a new layout with the &lt;b&gt;New layout&lt;/b&gt; menu action or button (%1). This clears the existing layout.</source>
        <translation>Die vorgefundene Partitionierung des Laufwerkes kann durch das Installationsprogramm nicht verändert werden. Um eine angepaßte Partitionierung zu erstellen, merken sie das Laufwerk für eine Neupartitionierung vor, indem sie die Menüoption &lt;b&gt;Neupartitionierung&lt;/b&gt; wählen oder die Schaltfläche (%1) drücken. Die vorgefundenen Partitionen des Laufwerkes werden damit im weiteren Verlauf gelöscht.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="707"/>
        <source>Basic layout requirements</source>
        <translation>Grundvoraussetzungen der Aufteilung</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="708"/>
        <source>%1 requires a root partition. The swap partition is optional but highly recommended. If you want to use the Suspend-to-Disk feature of %1, you will need a swap partition that is larger than your physical memory size.</source>
        <translation>%1 benötigt eine Wurzelpartition. Die SWAP-Partition ist optional, wird aber empfohlen. Falls die Verwendung der Suspend-to-Disk Funktion von %1 gewünscht ist , ist eine SWAP-Partition erforderlich, die größer als die physische RAM-Speichergröße ist.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="710"/>
        <source>If you choose a separate /home partition it will be easier for you to upgrade in the future, but this will not be possible if you are upgrading from an installation that does not have a separate home partition.</source>
        <translation>Bei einer Entscheidung für eine separate /home-Partition ist eine Aktualisierung der Distributionsversion einfacher. allerdings ist es dann unmöglich, auf eine installation zu aktualisieren, die keine separate /home-Partition hat.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="712"/>
        <source>Active partition</source>
        <translation>Aktive Partition</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="713"/>
        <source>For the installed operating system to boot, the appropriate partition (usually the boot or root partition) must be the marked as active.</source>
        <translation>Um das installierte Betriebssystem starten zu können, muß die entsprechende Partition (üblicherweise die boot- oder root-Partition) als »aktiv« markiert sein.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="714"/>
        <source>The active partition of a drive can be chosen using the &lt;b&gt;Active partition&lt;/b&gt; menu action.</source>
        <translation>Die aktive Partition eines Laufwerkes kann mittels des Menüs &lt;b&gt;Aktive Partition&lt;/b&gt; festgelegt werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="715"/>
        <source>A partition with an asterisk (*) next to its device name is, or will become, the active partition.</source>
        <translation>Ist eine Partition mit einem Sternchen (*) hinter ihrer Bezeichnung markiert, ist sie aktiv oder wird aktiviert.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="720"/>
        <source>Boot partition</source>
        <translation>Bootpartition</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="721"/>
        <source>This partition is generally only required for root partitions on virtual devices such as encrypted, LVM or software RAID volumes.</source>
        <translation>Diese Partition wird im allgemeinen nur benötigt, wenn sich die »root«-Partition auf einem virtuellen Gerät, wie z.B. auf einem verschlüsselten Laufwerk, »LVM« oder in einem »Software-RAID« Verbund befindet.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="722"/>
        <source>It contains a basic kernel and drivers used to access the encrypted disk or virtual devices.</source>
        <translation>Es nimmt einen Basiskernel sowie Treiber, die für den Zugriff auf verschlüsselte Laufwerke oder virtuelle Geräte benötigt werden, auf.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="723"/>
        <source>BIOS-GRUB partition</source>
        <translation>BIOS-GRUB Partition</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="724"/>
        <source>When using a GPT-formatted drive on a non-EFI system, a 1MB BIOS boot partition is required when using GRUB.</source>
        <translation>Wenn ein mit »GPT« formatiertes Laufwerk auf einem System ohne »EFI« genutzt werden soll, wird bei Verwendung von »GRUB« eine mindestens 1 MB große »BIOS boot« Partition benötigt.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="725"/>
        <source>New drives are formatted in GPT if more than 4 partitions are to be created, or the drive has a capacity greater than 2TB. If the installer is about to format the disk in GPT, and there is no BIOS-GRUB partition, a warning will be displayed before the installation starts.</source>
        <translation>Neue Laufwerke werden mit »GPT« formatiert, wenn sie mehr als 4 Partitionen umfassen, oder das Laufwerk eine Größe von mehr als 2 TB hat. Falls das Installationsprogramm eine Formatierung mit »GPT« für das Laufwerk vornimmt, und keine »GRUB-BIOS« Partition angelegt ist, wird eine Warnung vor dem Beginn der Installation angezeigt.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="727"/>
        <source>Need help creating a layout?</source>
        <translation>Benötigen sie Hilfe beim Einrichten der Aufteilung?</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="728"/>
        <source>Just right-click on a drive and select &lt;b&gt;Layout Builder&lt;/b&gt; from the menu. This can create a layout similar to that of the regular install.</source>
        <translation>Klicken sie mit der rechten Maustaste auf ein Laufwerk und wählen Sie aus dem Menü&lt;b&gt;Layout Erstellung&lt;/b&gt;. Dadurch kann ein ähnliches Layout wie bei der regulären Installation erstellt werden</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="762"/>
        <source>Install GRUB for Linux and Windows</source>
        <translation>GRUB für Linux und Windows installieren</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="763"/>
        <source>%1 uses the GRUB bootloader to boot %1 and Microsoft Windows.</source>
        <translation>%1 verwendet den GRUB-Bootloader zum Booten von %1 und Microsoft Windows.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="764"/>
        <source>By default GRUB is installed in the Master Boot Record (MBR) or ESP (EFI System Partition for 64-bit UEFI boot systems) of your boot drive and replaces the boot loader you were using before. This is normal.</source>
        <translation>Standardmäßig wird GRUB im Master Boot Record (MBR) oder ESP (EFI System Partition für 64-Bit-UEFI-Boot-Systeme) Ihres Bootlaufwerks installiert und ersetzt den zuvor verwendeten Bootloader.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="765"/>
        <source>If you choose to install GRUB to Partition Boot Record (PBR) instead, then GRUB will be installed at the beginning of the specified partition. This option is for experts only.</source>
        <translation>Wenn sie sich stattdessen für die Installation von GRUB in den Partition Boot Record (PBR) entscheiden, wird GRUB am Anfang der angegebenen Partition installiert. Diese Option ist nur für Experten geeignet.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="766"/>
        <source>If you uncheck the Install GRUB box, GRUB will not be installed at this time. This option is for experts only.</source>
        <translation>Wenn Sie das Kontrollkästchen GRUB installieren deaktivieren, wird GRUB zu diesem Zeitpunkt nicht installiert. Diese Option ist nur für Experten geeignet.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="767"/>
        <source>Create a swap file</source>
        <translation>Eine Auslagerungsdatei (swap) erstellen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="768"/>
        <source>A swap file is more flexible than a swap partition; it is considerably easier to resize a swap file to adapt to changes in system usage.</source>
        <translation>Eine Auslagerungsdatei ist flexibler als eine Auslagerungspartition; es ist wesentlich einfacher, die Größe einer Auslagerungsdatei zu ändern, um sie an Änderungen der Systemnutzung anzupassen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="769"/>
        <source>By default, this is checked if no swap partitions have been set, and unchecked if swap partitions are set. This option should be left untouched, and is for experts only.</source>
        <translation>Standardmäßig ist diese Option aktiviert, wenn keine Swap-Partitionen eingerichtet wurden, und deaktiviert, wenn Swap-Partitionen eingerichtet sind. Diese Option sollte unangetastet bleiben und ist nur für Experten gedacht.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="770"/>
        <source>Setting the size to 0 has the same effect as unchecking this option.</source>
        <translation>Die Einstellung der Größe auf 0 hat die gleiche Wirkung wie die Deaktivierung dieser Option.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="877"/>
        <source>Enjoy using %1</source>
        <translation>Viel Spaß bei der Verwendung von %1</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="729"/>
        <source>Upgrading</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="675"/>
        <source>Format without mounting</source>
        <translation>Formatieren ohne anschließendes Einhängen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="676"/>
        <source>BIOS Boot GPT partition for GRUB</source>
        <translation>BIOS Boot GPT Partition für GRUB</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="677"/>
        <location filename="../minstall.cpp" line="716"/>
        <source>EFI System Partition</source>
        <translation>EFI-Systempartition</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="679"/>
        <source>Boot manager</source>
        <translation>Boot-Manager</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="680"/>
        <source>System root</source>
        <translation>Wurzelverzeichnis</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="681"/>
        <source>User data</source>
        <translation>Benutzerdaten</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="682"/>
        <source>Static data</source>
        <translation>Stammdaten</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="683"/>
        <source>Variable data</source>
        <translation>Veränderliche Daten</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="684"/>
        <source>Temporary files</source>
        <translation>Temporäre Dateien</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="685"/>
        <source>Swap files</source>
        <translation>Auslagerungsdatei</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="686"/>
        <source>Swap partition</source>
        <translation>Auslagerungspartition</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="693"/>
        <source>Selecting &lt;b&gt;Preserve /home&lt;/b&gt; for the root partition preserves the contents of the /home directory, deleting everything else. This option can only be used when /home is on the same partition as the root partition.</source>
        <translation>Die Wahl von &lt;b&gt;Preserve /home&lt;/b&gt; für die root-Partition bewahrt den Inhalt des  /home-Verzeichnisses und löscht alles andere. Diese Option kann nur gewählt werden, wenn sich /home auf derselben Partition mit root befindet.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="717"/>
        <source>If your system uses the Extensible Firmware Interface (EFI), a partition known as the EFI System Partition (ESP) is required for the system to boot.</source>
        <translation>Wenn das System das Extensible Firmware Interface (EFI) verwendet, ist eine Partition mit der Bezeichnung EFI System Partition (ESP) erforderlich, damit das System booten kann.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="718"/>
        <source>These systems do not require any partition marked as Active, but instead require a partition formatted with a FAT file system, marked as an ESP.</source>
        <translation>Diese Systeme benötigen keine als aktiv gekennzeichnete Partition, sondern eine mit einem FAT-Dateisystem formatierte Partition, die als ESP gekennzeichnet ist.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="719"/>
        <source>Most systems built within the last 10 years use EFI.</source>
        <translation>Die meisten Systeme, die in den letzten 10 Jahren gebaut wurden, verwenden EFI.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="730"/>
        <source>To upgrade from an existing Linux installation, select the same home partition as before and select &lt;b&gt;Preserve&lt;/b&gt; as the format.</source>
        <translation>Bei einer Installation, die über einem vorhandenen Linux erfolgt, kann die bestehende »home« Partition ausgewählt werden. Ihr Inhalt kann durch Einstellung des Formates auf &lt;b&gt;Beibehalten&lt;/b&gt; übernommen werden kann.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="731"/>
        <source>If you do not use a separate home partition, select &lt;b&gt;Preserve /home&lt;/b&gt; on the root file system entry to preserve the existing /home directory located on your root partition. The installer will only preserve /home, and will delete everything else. As a result, the installation will take much longer than usual.</source>
        <translation>Falls sie sich dafür entscheiden, keine separate »home« Partition zu verwenden, wählen sie bitte die Formatierungsoption &lt;b&gt;»/home« Partiton beibehalten&lt;/b&gt; für das »root« Dateisystem. Mit dieser Einstellung wird das Installationsprogramm die vorhandene »/home« Partition nicht antasten, wohingegen alles übrige gelöscht wird. Die Installation wird dadurch mehr Zeit beanspruchen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="733"/>
        <source>Preferred Filesystem Type</source>
        <translation>Bervorzugter Dateisystemtyp</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="734"/>
        <source>For %1, you may choose to format the partitions as ext2, ext3, ext4, f2fs, jfs, xfs or btrfs.</source>
        <translation>Für %1 können sie wählen, die Partitionen als ext2, ext3, ext4, f2fs, jfs, xfs oder btrfs zu formatieren.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="735"/>
        <source>Additional compression options are available for drives using btrfs. Lzo is fast, but the compression is lower. Zlib is slower, with higher compression.</source>
        <translation>Für Laufwerke mit btrfs stehen zusätzliche Kompressionsoptionen zur Verfügung. Zlib ist langsamer, bietet aber einen höheren Kompressionsfaktor.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="737"/>
        <source>System partition management tool</source>
        <translation>Partitionsverwaltungsprogramm des Systems</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="738"/>
        <source>For more control over the drive layouts (such as modifying the existing layout on a disk), click the partition management button (%1). This will run the operating system&apos;s partition management tool, which will allow you to create the exact layout you need.</source>
        <translation>Um die Laufwerksaufteilung bedarfsgerecht anpassen zu können (einschl. Veränderungen an bestehenden Partitionen), klicken sie auf die Schaltfläche zur Partitionsverwaltung (%1). Damit wird das betriebssystemeigene Partitionierunsprogramm gestartet, mit dem detaillierte Anpassungen vorgenommen werden können.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="744"/>
        <source>To preserve an encrypted partition, right-click on it and select &lt;b&gt;Unlock&lt;/b&gt;. In the dialog that appears, enter a name for the virtual device and the password. When the device is unlocked, the name you chose will appear under &lt;i&gt;Virtual Devices&lt;/i&gt;, with similar options to that of a regular partition.</source>
        <translation>Um eine bestehende verschlüsselte Partition zu übernehmen, klicken sie mit der rechten Maustaste darauf und wählen sie &lt;b&gt;Aufschließen&lt;/b&gt;. Im darauffolgenden Dialogfenster geben sie einen Namen für das virtuelle Gerät und sein Paßwort an. Nachdem das Gerät entschlüsselt wurde, erscheint es mit dem von Ihnen gewählten Namen unter &lt;i&gt;Virtuelle Geräte&lt;/i&gt; und mit Optionen ähnlich jenen normaler Partitionen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="746"/>
        <source>For the encrypted partition to be unlocked at boot, it needs to be added to the crypttab file. Use the &lt;b&gt;Add to crypttab&lt;/b&gt; menu action to do this.</source>
        <translation>Damit eine verschlüsselte Partition beim booten entschlüsselt wird, muß sie zur »crypttab« Datei hinzugefügt werden. Verwenden sie dazu den Menüeintrag &lt;b&gt;Zu »crypttab« hinzufügen&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="747"/>
        <source>Other partitions</source>
        <translation>Andere Partitionen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="748"/>
        <source>The installer allows other partitions to be created or used for other purposes, however be mindful that older systems cannot handle drives with more than 4 partitions.</source>
        <translation>Das Installationsprogramm erlaubt es, weitere Partitionen zu erstellen oder für andere Zwecke zu verwenden. Beachten sie jedoch, dass ältere Systeme keine Laufwerke mit mehr als 4 Partitionen verarbeiten können.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="749"/>
        <source>Subvolumes</source>
        <translation>Untersektionen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="750"/>
        <source>Some file systems, such as Btrfs, support multiple subvolumes in a single partition. These are not physical subdivisions, and so their order does not matter.</source>
        <translation>Einige Dateisysteme, wie z.B. Btrfs, unterstützen die Einrichtung mehrerer Untersektionen auf einer einzelnen Partition. Hier handelt es sich nicht um physikalische Unterteilungen, daher hat ihre Reihenfolge keine Bedeutung.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="752"/>
        <source>Use the &lt;b&gt;Scan subvolumes&lt;/b&gt; menu action to search an existing Btrfs partition for subvolumes. To create a new subvolume, use the &lt;b&gt;New subvolume&lt;/b&gt; menu action.</source>
        <translation>Der Menüeintrag &lt;b&gt;Nach Untersektionen suchen&lt;/b&gt; gestattet die Suche nach vorhandenen Btrfs-Untersektionen. Mit dem Menüeintrag &lt;b&gt;Neue Untersektion&lt;/b&gt; kann eine zusätzliche Untersektion angelegt werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="754"/>
        <source>Existing subvolumes can be preserved, however the name must remain the same.</source>
        <translation>Vorhandene Untersektionen können beibehalten werden. Ihr Name muß dabei unverändert bleiben.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="755"/>
        <source>Virtual Devices</source>
        <translation>Virtuelle Geräte</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="756"/>
        <source>If the intaller detects any virtual devices such as opened LUKS partitions, LVM logical volumes or software-based RAID volumes, they may be used for the installation.</source>
        <translation>Wenn das Installationsprogramm virtuelle Geräte wie z.B. eine LUKS Partition, logische LVM Laufwerke oder einen softwarebasiertes RAID-Verbund erkennt, können diese für die Installation genutzt werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="757"/>
        <source>The use of virtual devices (beyond preserving encrypted file systems) is an advanced feature. You may have to edit some files (eg. initramfs, crypttab, fstab) to ensure the virtual devices used are created upon boot.</source>
        <translation>Die Verwendung virtueller Geräte (jenseits des einfachen Beibehaltens einer verschlüsselten Partition) ist eine komplexere Angelegenheit. Es ist möglich, daß einige Dateien (z.B. initramfs, crypttab, fstab) manuell bearbeitet werden müssen, um sicherzustellen daß die verwendeten virtuellen Geräte beim Bootprozess initiiert werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="776"/>
        <source>&lt;p&gt;&lt;b&gt;Common Services to Enable&lt;/b&gt;&lt;br/&gt;Select any of these common services that you might need with your system configuration and the services will be started automatically when you start %1.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Systemdienste einschalten&lt;/b&gt;&lt;br/&gt;
Wählen sie hier die Systemdienste, die sie in ihrer Systemkonfiguration haben wollen und die beim Start von %1 automatisch gestartet werden sollen.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="780"/>
        <source>&lt;p&gt;&lt;b&gt;Computer Identity&lt;/b&gt;&lt;br/&gt;The computer name is a common unique name which will identify your computer if it is on a network. The computer domain is unlikely to be used unless your ISP or local network requires it.&lt;/p&gt;&lt;p&gt;The computer and domain names can contain only alphanumeric characters, dots, hyphens. They cannot contain blank spaces, start or end with hyphens&lt;/p&gt;&lt;p&gt;The SaMBa Server needs to be activated if you want to use it to share some of your directories or printer with a local computer that is running MS-Windows or Mac OSX.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Computer Identifikation&lt;/b&gt;&lt;br/&gt;Der “Computername” ist ein gewöhnlicher, nicht mehrfach vergebener Name, der Ihren Computer, falls er einem Netzwerk angeschlossen ist, eindeutig identifiziert. Der “Domainname” muß auf dem Computer nur eingetragen werden, falls Ihr Internetdienstleister oder das lokale Netzwerk dies erfordern.&lt;/p&gt;&lt;p&gt;Die Computer- und Domainnamen dürfen nur alphanumerische Zeichen, Punkte und Bindestriche enthalten. Sie dürfen keine Leerzeichen enthalten und nicht mit einem Bindestrich beginnen oder enden.&lt;/p&gt;&lt;p&gt;Der “SaMBa” Server muss aktiviert werden, sofern sie beabsichtigen, einige Ihrer Verzeichnisse oder Drucker mit anderen Computern gemeinsam zu nutzen, die mit Betriebssystemen wie “MS-Windows” oder “Mac OS X” arbeiten.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="790"/>
        <source>Localization Defaults</source>
        <translation>Standardlokalisierung</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="791"/>
        <source>Set the default locale. This will apply unless they are overridden later by the user.</source>
        <translation>Auswahl der Standardlokalisation. Sie wird verwendet, solange sie nicht später durch den Nutzer geändert wird.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="792"/>
        <source>Configure Clock</source>
        <translation>Uhrzeit einstellen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="793"/>
        <source>If you have an Apple or a pure Unix computer, by default the system clock is set to Greenwich Meridian Time (GMT) or Coordinated Universal Time (UTC). To change this, check the &quot;&lt;b&gt;System clock uses local time&lt;/b&gt;&quot; box.</source>
        <translation>Falls sie einen Computer, auf dem ausschließlich Unix betrieben wurde oder einen “Apple” Computer verwenden, ist dessen Systemuhr auf die koordinierte Weltzeit (UTC) eingestellt. Um dies zu ändern, setzen sie einen Haken bei &quot;&lt;b&gt;Systemuhr verwendet Lokalzeit&lt;/b&gt;&quot;. </translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="795"/>
        <source>The system boots with the timezone preset to GMT/UTC. To change the timezone, after you reboot into the new installation, right click on the clock in the Panel and select Properties.</source>
        <translation>Das System startet mit der Zeitzoneneinstellung “Koordinierte Weltzeit (UTC)&quot;. Sie können die Zeitzoneneinstellung nach dem ersten Start der neuen Installation ändern, indem sie mit der rechten Maustaste auf die Uhr in der Statuszeile klicken und aus dem Kontextmenü den Eintrag “Eigenschaften” wählen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="797"/>
        <source>Service Settings</source>
        <translation>Wartungseinstellungen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="798"/>
        <source>Most users should not change the defaults. Users with low-resource computers sometimes want to disable unneeded services in order to keep the RAM usage as low as possible. Make sure you know what you are doing!</source>
        <translation>Für die Mehrheit der Nutzer sollte keine Änderung der Standardeinstellungen erforderlich sein. Für Nutzer von Computern mit geringer Leistungsfähigkeit oder wenig Arbeitsspeicher (RAM) kann es sinnvoll sein, nicht benötigte Dienste zu deaktivieren, um die Prozessorlast und den  Speicherplatzverbrauch im RAM  gering zu halten. Nehmen sie Änderungen nur vor, wenn sie deren Auswirkungen kennen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="804"/>
        <source>Default User Login</source>
        <translation>Standard-Benutzeranmeldung</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="805"/>
        <source>The root user is similar to the Administrator user in some other operating systems. You should not use the root user as your daily user account. Please enter the name for a new (default) user account that you will use on a daily basis. If needed, you can add other user accounts later with %1 User Manager.</source>
        <translation>Der Benutzer root ist dem Administrator anderer Betriebssysteme vergleichbar. Dieser spezielle Nutzerzugang sollte nie für die normale, alltägliche Nutzung des Computers verwendet werden. Bitte geben sie einen Namen für ein neues (Standard-)Benutzerkonto ein, das sie im normalen Betrieb verwenden werden. Falls erforderlich, können später weitere Benutzerkonten in der %1 Benutzerverwaltung hinzugefügt werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="809"/>
        <source>Passwords</source>
        <translation>Passwörter</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="810"/>
        <source>Enter a new password for your default user account and for the root account. Each password must be entered twice.</source>
        <translation>Geben sie ein neues Passwort für ihr Standardbenutzerkonto und für das Administratorkonto ein. Jedes Passwort muss zweimal eingegeben werden.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="812"/>
        <source>No passwords</source>
        <translation>Keine Passwörter</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="813"/>
        <source>If you want the default user account to have no password, leave its password fields empty. This allows you to log in without requiring a password.</source>
        <translation>Falls kein Passwort für den Standardbenutzerzugang gewünscht wird, können die entsprechenden Passwortfelder leer bleiben, Der Anmeldevorgang erfolgt in diesem Fall ohne Passwortabfrage.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="815"/>
        <source>Obviously, this should only be done in situations where the user account does not need to be secure, such as a public terminal.</source>
        <translation>Diese Option sollte selbstverständlich nur aktiviert werden, wenn es nicht auf die Sicherheit persönlicher Daten des Nutzerkontos ankommt, z.B. bei öffentlichen Computerterminals.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="823"/>
        <source>Old Home Directory</source>
        <translation>Altes /home-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="824"/>
        <source>A home directory already exists for the user name you have chosen. This screen allows you to choose what happens to this directory.</source>
        <translation>Für den ausgewählten Benutzernamen gibt es bereits ein /home-Verzeichnis. Hier kann jetzt festgelegt werden, was mit diesem Verzeichnis geschehen soll.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="826"/>
        <source>Re-use it for this installation</source>
        <translation>Wiederverwenden für diese Neuinstallation.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="827"/>
        <source>The old home directory will be used for this user account. This is a good choice when upgrading, and your files and settings will be readily available.</source>
        <translation>Das alte /home-Verzeichnis wird fur dieses Benutzerkonto verwendet. Das ist bei einer Aktualisierungsinstallation eine gute Wahl und Dateien und Voreinstellungen stehen dann sofort zur Verfügung.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="829"/>
        <source>Rename it and create a new directory</source>
        <translation>Verzeichnis umbenennen und neues Verzeichnis anlegen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="830"/>
        <source>A new home directory will be created for the user, but the old home directory will be renamed. Your files and settings will not be immediately visible in the new installation, but can be accessed using the renamed directory.</source>
        <translation>Fur den Benutzer wird ein neues /home-Verzeichnis angelegt, das alte wird dafür umbenannt. Dateien und Voreinstellungen sind dann nicht sofort in der Neuinstallation sichtbar, werden durch Verwendung des umbenannten Verzeichnisses aber erreichbar. </translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="832"/>
        <source>The old directory will have a number at the end of it, depending on how many times the directory has been renamed before.</source>
        <translation>Das alte Verzeichnis enthält am Ende des Namens eine Nummer in Abhängigkeit von der Anzahl der vorhergehenden Umbenennungen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="833"/>
        <source>Delete it and create a new directory</source>
        <translation>Verzeichnis löschen und neues Verzeichnis anlegen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="834"/>
        <source>The old home directory will be deleted, and a new one will be created from scratch.</source>
        <translation>Das alte Verzeichnis wird gelöscht und ein komplett neues wird angelegt.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="835"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="836"/>
        <source>All files and settings will be deleted permanently if this option is selected. Your chances of recovering them are low.</source>
        <translation>Alle Dateien und Voreinstellung werden bei AUswahl dieser Option dauerhaft gelöscht. Die Chancen einer Wiederherstellung sind gering.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="852"/>
        <source>Installation in Progress</source>
        <translation>Installation wird durchgeführt</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="853"/>
        <source>%1 is installing. For a fresh install, this will probably take 3-20 minutes, depending on the speed of your system and the size of any partitions you are reformatting.</source>
        <translation>%1 wird gerade installiert. Eine komplette Neuinstallation benötigt etwa zwischen 3-20 Minuten in Abhängigkeit von der Leistungsfähigkeit des Rechners und der Größe der zu formatierenden Partitionen.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="855"/>
        <source>If you click the Abort button, the installation will be stopped as soon as possible.</source>
        <translation>Wenn sie auf die Schaltfläche Abbrechen klicken, wird die Installation so schnell wie möglich gestoppt.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="857"/>
        <source>Change settings while you wait</source>
        <translation>Ändern sie die Einstellungen, während sie warten</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="858"/>
        <source>While %1 is being installed, you can click on the &lt;b&gt;Next&lt;/b&gt; or &lt;b&gt;Back&lt;/b&gt; buttons to enter other information required for the installation.</source>
        <translation>Während %1 installiert wird, können sie auf die Schaltflächen &lt;b&gt;Weiter&lt;/b&gt; oder &lt;b&gt;Zurück&lt;/b&gt; klicken, um weitere für die Installation erforderliche Informationen einzugeben.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="860"/>
        <source>Complete these steps at your own pace. The installer will wait for your input if necessary.</source>
        <translation>Führen sie diese Schritte in ihrem eigenen Tempo durch. Das Installationsprogramm wartet bei Bedarf auf ihre Eingaben.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="868"/>
        <source>&lt;p&gt;&lt;b&gt;Congratulations!&lt;/b&gt;&lt;br/&gt;You have completed the installation of %1&lt;/p&gt;&lt;p&gt;&lt;b&gt;Finding Applications&lt;/b&gt;&lt;br/&gt;There are hundreds of excellent applications installed with %1 The best way to learn about them is to browse through the Menu and try them. Many of the apps were developed specifically for the %1 project. These are shown in the main menus. &lt;p&gt;In addition %1 includes many standard Linux applications that are run only from the command line and therefore do not show up in the Menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Glückwunsch!&lt;/b&gt;&lt;br/&gt;Sie haben die Installation von %1 Linux erfolgreich durchgeführt.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Programme suchen und finden&lt;/b&gt;&lt;br/&gt;Es gibt Hunderte von hervorragenden Programmen, die mit %1 installiert wurden. Der schnellste Weg sie zu finden führt über das Menü. Durchforsten sie es und probieren sie die Programme aus. Viele Programme wurden speziell für das %1 Projekt entwickelt. Diese finden sie in den Hauptmenüs. &lt;p&gt;Zusätzlich beinhaltet %1 viele Standard-Linux-Programme, die nur in einer Textkonsole laufen und daher nicht im Menü erscheinen.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="878"/>
        <location filename="../minstall.cpp" line="1144"/>
        <source>&lt;p&gt;&lt;b&gt;Support %1&lt;/b&gt;&lt;br/&gt;%1 is supported by people like you. Some help others at the support forum - %2 - or translate help files into different languages, or make suggestions, write documentation, or help test new software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Unterstützung für %1&lt;/b&gt;&lt;br/&gt;%1 wird von Leuten wie ihnen unterstützt. Einige helfen anderen Nutzern im Forum - %2 - oder übersetzen Dateien in andere Sprachen oder machen Verbesserungsvorschläge, schreiben Dokumentationen oder helfen beim Testen neuer Programme.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="908"/>
        <source>Finish</source>
        <translation>Abschließen</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="911"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="913"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="423"/>
        <source>Configuring sytem. Please wait.</source>
        <translation>System wird konfiguriert. Bitte warten sie.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="430"/>
        <source>Configuration complete. Restarting system.</source>
        <translation>Konfiguration abgeschlossen. System wird neu gestartet.</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1066"/>
        <source>The installation and configuration is incomplete.
Do you really want to stop now?</source>
        <translation>Die Installation und die Konfiguration sind unvollständig.
Möchten sie wirklich den Vorgang beenden?</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1130"/>
        <source>&lt;p&gt;&lt;b&gt;Getting Help&lt;/b&gt;&lt;br/&gt;Basic information about %1 is at %2.&lt;/p&gt;&lt;p&gt;There are volunteers to help you at the %3 forum, %4&lt;/p&gt;&lt;p&gt;If you ask for help, please remember to describe your problem and your computer in some detail. Usually statements like &apos;it didn&apos;t work&apos; are not helpful.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Wo gibt es Hilfe?&lt;/b&gt;&lt;br/&gt;Grundlegende Informationen zu %1 gibt es auf %2. &lt;/p&gt;&lt;p&gt;Viele Freiwillige helfen im %3 Forum, %4.&lt;/p&gt;&lt;p&gt;Wenn sie um Hilfe bitten, denken sie bitte daran, eine genaue Problembeschreibung anzugeben und Details zu Ihrer Hardware-Ausstattung mitzuteilen. Angaben wie: &apos;Es funktioniert nicht&apos; sind dabei nicht besonders hilfreich.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1138"/>
        <source>&lt;p&gt;&lt;b&gt;Repairing Your Installation&lt;/b&gt;&lt;br/&gt;If %1 stops working from the hard drive, sometimes it&apos;s possible to fix the problem by booting from LiveDVD or LiveUSB and running one of the included utilities in %1 or by using one of the regular Linux tools to repair the system.&lt;/p&gt;&lt;p&gt;You can also use your %1 LiveDVD or LiveUSB to recover data from MS-Windows systems!&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Reparatur der Installation&lt;/b&gt;&lt;br/&gt;Falls %1 von der Festplatte nicht mehr startet oder nicht mehr richtig funktioniert, ist es oftmals möglich das Problem dadurch zu lösen, dass man von LiveDVD oder LiveUSB startet und eines der Programme zur Systemkonfiguration von %1 oder eines der regulären Linux-Programme benutzt, um das System zu reparieren.&lt;/p&gt;&lt;p&gt;Sie können Ihre %1 LiveDVD oder LiveUSB auch benutzen, um Daten von MS-Windows-Systemen zu retten!&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1152"/>
        <source>&lt;p&gt;&lt;b&gt;Adjusting Your Sound Mixer&lt;/b&gt;&lt;br/&gt; %1 attempts to configure the sound mixer for you but sometimes it will be necessary for you to turn up volumes and unmute channels in the mixer in order to hear sound.&lt;/p&gt; &lt;p&gt;The mixer shortcut is located in the menu. Click on it to open the mixer. &lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Einstellung des Sound Mixers&lt;/b&gt;&lt;br/&gt; %1 versucht, den Sound Mixer für sie einzustellen, aber manchmal ist es notwendig Lautstärke-Einstellungen anzupassen oder abgeschaltete Kanäle im Mixer einzuschalten, um etwas hören zu können.&lt;/p&gt;&lt;p&gt;Ein Mixer-Schnellstart-Symbol befindet sich in der Kontrollleiste. Um den Mixer zu öffnen, einfach darauf klicken. &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1160"/>
        <source>&lt;p&gt;&lt;b&gt;Keep Your Copy of %1 up-to-date&lt;/b&gt;&lt;br/&gt;For more information and updates please visit&lt;/p&gt;&lt;p&gt; %2&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Halten sie ihr %1 stets aktuell&lt;/b&gt;&lt;br/&gt;Für Aktualisierungen und weitere Informationen besuchen sie bitte &lt;/p&gt;&lt;p&gt;%2.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../minstall.cpp" line="1165"/>
        <source>&lt;p&gt;&lt;b&gt;Special Thanks&lt;/b&gt;&lt;br/&gt;Thanks to everyone who has chosen to support %1 with their time, money, suggestions, work, praise, ideas, promotion, and/or encouragement.&lt;/p&gt;&lt;p&gt;Without you there would be no %1.&lt;/p&gt;&lt;p&gt;%2 Dev Team&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Danksagung&lt;/b&gt;&lt;br/&gt;Herzlichen Dank an alle, die %1 unterstützen mit Zeit, Geld, Vorschlägen, Arbeit, Lob, Ideen, Weiterempfehlungen, und/oder Zuspruch.&lt;/p&gt;&lt;p&gt;Ohne Euch gäbe es kein %1.&lt;/p&gt;&lt;p&gt;%2 Dev Team&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MeInstall</name>
    <message>
        <location filename="../meinstall.ui" line="43"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="51"/>
        <source>Live Log</source>
        <translation>Live-Protokoll</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="160"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="86"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="177"/>
        <source>Gathering Information, please stand by.</source>
        <translation>Sammle Informationen. Bitte warten.</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="209"/>
        <source>Terms of Use</source>
        <translation>Nutzungsbedingungen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="297"/>
        <source>Change Keyboard Settings</source>
        <translation>Tastatur-Einstellungen ändern</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="375"/>
        <source>Select type of installation</source>
        <translation>Art der Installation auswählen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="387"/>
        <source>Regular install using the entire disk</source>
        <translation>Normale Installation (verwendet den gesamten Datenträger)</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="400"/>
        <source>Customize the disk layout</source>
        <translation>Aufteilung des Datenträgers anpassen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="413"/>
        <source>Use disk:</source>
        <translation>Folgenden Datenträger benutzen:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="567"/>
        <source>Encrypt</source>
        <translation>Verschlüsseln</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="579"/>
        <location filename="../meinstall.ui" line="782"/>
        <source>Encryption password:</source>
        <translation>Verschlüsselungspasswort:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="596"/>
        <location filename="../meinstall.ui" line="799"/>
        <source>Confirm password:</source>
        <translation>Passwort bestätigen:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="447"/>
        <source>Root</source>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="241"/>
        <source>Keyboard Settings</source>
        <translation>Tastatur-Einstellungen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="256"/>
        <source>Model:</source>
        <translation>Modell:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="271"/>
        <source>Variant:</source>
        <translation>Variante:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="309"/>
        <source>Layout:</source>
        <translation>Layout:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="457"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="532"/>
        <location filename="../meinstall.ui" line="554"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="616"/>
        <location filename="../meinstall.ui" line="1057"/>
        <source>Enable hibernation support</source>
        <translation>Unterstützung für den Ruhezustand aktivieren</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="654"/>
        <source>Choose partitions</source>
        <translation>Partitionen wählen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="694"/>
        <source>Query the operating system and reload the layouts of all drives.</source>
        <translation>Abfrage des Betriebssystems und erneutes Einlesen der Aufteilung aller Laufwerke</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="727"/>
        <source>Remove an existing entry from the layout. This only works with entries to a new layout.</source>
        <translation>Entferne eine zur Neuanlage vorgemerkte Partition aus der zusammengestellten Laufwerksstruktur</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="716"/>
        <source>Add a new partition entry. This only works with a new layout.</source>
        <translation>Neue Partition hinzufügen (nur möglich in Verbindung mit Änderungen an der Aufteilung des Datenträgers)</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="663"/>
        <source>Show Grid</source>
        <translation>Raster anzeigen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="670"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="680"/>
        <source>Mark the selected drive to be cleared for a new layout.</source>
        <translation>Das ausgewählte Laufwerk zum Löschen und Neupartitionieren vormerken.</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="705"/>
        <source>Run the partition management application of this operating system.</source>
        <translation>Starte das Partitionsverwaltungsprogramm dieses Betriebssystems</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="776"/>
        <source>Encryption options</source>
        <translation>Verschlüsselungsoptionen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="835"/>
        <source>Install GRUB for Linux and Windows</source>
        <translation>GRUB für Linux und Windows installieren</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="859"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="865"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="868"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="890"/>
        <source>EFI System Partition</source>
        <translation>EFI-Systempartition</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="893"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="925"/>
        <source>Partition Boot Record</source>
        <translation>Partition Boot Record</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="928"/>
        <source>PBR</source>
        <translation>PBR</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="941"/>
        <source>System boot disk:</source>
        <translation>Systemstart-Laufwerk:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="954"/>
        <source>Location to install on:</source>
        <translation>Installationsort:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="990"/>
        <source>Create a swap file</source>
        <translation>Eine Auslagerungsdatei (swap) erstellen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1007"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1027"/>
        <source>Size:</source>
        <translation>Größe:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1050"/>
        <source>Location:</source>
        <translation>Einsatzort:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1089"/>
        <source>Common Services to Enable</source>
        <translation>Übliche Dienste einschalten</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1108"/>
        <source>Service</source>
        <translation>Dienst</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1113"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1146"/>
        <source>Computer Network Names</source>
        <translation>Computer Netzwerknamen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1173"/>
        <source>Workgroup</source>
        <translation>Workgroup</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1186"/>
        <source>Workgroup:</source>
        <translation>Arbeitsgruppe:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1199"/>
        <source>SaMBa Server for MS Networking</source>
        <translation>Samba Server für MS-Netzwerk</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1215"/>
        <source>example.dom</source>
        <translation>beispiel.dom</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1228"/>
        <source>Computer domain:</source>
        <translation>Computer-Domain:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1254"/>
        <source>Computer name:</source>
        <translation>Computername:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1315"/>
        <source>Configure Clock</source>
        <translation>Uhrzeit einstellen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1356"/>
        <source>Format:</source>
        <translation>Zeitformat:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1384"/>
        <source>Timezone:</source>
        <translation>Zeitzone:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1429"/>
        <source>System clock uses local time</source>
        <translation>Systemuhr ist auf Ortszeit eingestellt</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1461"/>
        <source>Localization Defaults</source>
        <translation>Standardlokalisierung</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1501"/>
        <source>Locale:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1523"/>
        <source>Service Settings (advanced)</source>
        <translation>Einstellungen von Diensten (fortgeschritten) </translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1541"/>
        <source>Adjust which services should run at startup</source>
        <translation>Startdienste einrichten</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1544"/>
        <source>View</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1589"/>
        <source>Desktop modifications made in the live environment will be carried over to the installed OS</source>
        <translation>Modifikationen, die im jetzt laufenden Live-System vorgenommen wurden, bleiben in der Ziel-Installation erhalten.</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1592"/>
        <source>Save live desktop changes</source>
        <translation>Änderungen der Live-Arbeitsoberfläche speichern</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1605"/>
        <source>Default User Account</source>
        <translation>Standard-Benutzerkonto</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1617"/>
        <source>Default user login name:</source>
        <translation>Standard-Benutzername für das Anmelden</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1649"/>
        <source>Default user password:</source>
        <translation>Standard-Benutzerpasswort</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1678"/>
        <source>Confirm user password:</source>
        <translation>Benutzerpasswort bestätigen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1636"/>
        <source>username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1710"/>
        <source>Root (administrator) Account</source>
        <translation>root (Administrator)-Konto</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1728"/>
        <source>Root password:</source>
        <translation>Administrator-Passwort:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1757"/>
        <source>Confirm root password:</source>
        <translation>Administrator-Passwort bestätigen:</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1789"/>
        <source>Autologin</source>
        <translation>Automatisches Anmelden</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1825"/>
        <source>Existing Home Directory</source>
        <translation>Vorhandenes /home-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1834"/>
        <source>What would you like to do with the old directory?</source>
        <translation>Wie wollen sie mit dem alten Verzeichnis verfahren ?</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1841"/>
        <source>Re-use it for this installation</source>
        <translation>Wiederverwenden für diese Neuinstallation.</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1848"/>
        <source>Rename it and create a new directory</source>
        <translation>Verzeichnis umbenennen und neues Verzeichnis anlegen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1855"/>
        <source>Delete it and create a new directory</source>
        <translation>Löschen und neues Verzeichnis anlegen.</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1900"/>
        <source>Tips</source>
        <translation>Tipps</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1944"/>
        <source>Installation complete</source>
        <translation>Installation abgeschlossen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1950"/>
        <source>Automatically reboot the system when the installer is closed</source>
        <translation>Das System automatisch neu starten, wenn das Installationsprogramm geschlossen wird</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="1969"/>
        <source>Reminders</source>
        <translation>Erinnerungen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="72"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="79"/>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="122"/>
        <source>Installation in progress</source>
        <translation>Installation wird durchgeführt</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="137"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../meinstall.ui" line="140"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
</context>
<context>
    <name>Oobe</name>
    <message>
        <location filename="../oobe.cpp" line="331"/>
        <source>Please enter a computer name.</source>
        <translation>Bitte eine Bezeichnung für den Computer eingeben.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="335"/>
        <source>Sorry, your computer name contains invalid characters.
You'll have to select a different
name before proceeding.</source>
        <translation>Der Computername enthält leider ungültige Zeichen. 
Bitte wählen sie einen anderen Namen, 
bevor sie weitermachen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="340"/>
        <source>Please enter a domain name.</source>
        <translation>Bitte einen Domänennamen eingeben.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="344"/>
        <source>Sorry, your computer domain contains invalid characters.
You'll have to select a different
name before proceeding.</source>
        <translation>Der Domänenname des Computers enthält leider ungültige Zeichen.
Bitte wählen sie einen anderen Namen, bevor sie weitermachen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="351"/>
        <source>Please enter a workgroup.</source>
        <translation>Bitte eine Benutzergruppe eingeben.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="491"/>
        <source>The user name cannot contain special characters or spaces.
Please choose another name before proceeding.</source>
        <translation>Der Benutzername darf keine Sonderzeichen oder Leerzeichen enthalten.
Bitte wählen sie einen anderen Namen, bevor sie fortfahren.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="502"/>
        <source>Sorry, that name is in use.
Please select a different name.</source>
        <translation>Dieser Name existiert bereits.
Bitte einen anderen Namen wählen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="511"/>
        <source>You did not provide a passphrase for %1.</source>
        <translation>Sie haben keine Passphrase für %1 angegeben.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="512"/>
        <source>Are you sure you want to continue?</source>
        <translation>Sind sie sicher, daß sie fortfahren möchten?</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="518"/>
        <source>You did not provide a password for the root account. Do you want to continue?</source>
        <translation>Sie haben kein Passwort für das Administratorkonto angegeben. Möchten sie fortfahren?</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="531"/>
        <source>Failed to set user account passwords.</source>
        <translation>Festlegung des Benutzerpassworts fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="557"/>
        <source>Failed to save old home directory.</source>
        <translation>Speichern des alten Home-Verzeichnisses fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="566"/>
        <source>Failed to delete old home directory.</source>
        <translation>Löschen des alten Home-Verzeichnisses fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="587"/>
        <source>Sorry, failed to create user directory.</source>
        <translation>Konnte das Benutzer-Verzeichnis nicht anlegen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="590"/>
        <source>Sorry, failed to name user directory.</source>
        <translation>Konnte das Benutzer-Verzeichnis nicht umbenennen.</translation>
    </message>
    <message>
        <location filename="../oobe.cpp" line="626"/>
        <source>Failed to set ownership or permissions of user directory.</source>
        <translation>Eigentümer oder Berechtigungen des Benutzerverzeichnisses konnten nicht erstellt werden.</translation>
    </message>
</context>
<context>
    <name>PartMan</name>
    <message>
        <location filename="../partman.cpp" line="222"/>
        <source>Virtual Devices</source>
        <translation>Virtuelle Geräte</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="451"/>
        <location filename="../partman.cpp" line="511"/>
        <source>&amp;Add partition</source>
        <translation>&amp;Partition hinzufügen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="453"/>
        <source>&amp;Remove partition</source>
        <translation>&amp;Partition entfernen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="463"/>
        <source>&amp;Lock</source>
        <translation>&amp;Sperren</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="467"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Entsperren</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="471"/>
        <location filename="../partman.cpp" line="623"/>
        <source>Add to crypttab</source>
        <translation>Zu »crypttab« hinzufügen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="476"/>
        <source>Active partition</source>
        <translation>Aktive Partition</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="477"/>
        <source>EFI System Partition</source>
        <translation>EFI-Systempartition</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="485"/>
        <source>New subvolume</source>
        <translation>Neue Untersektion</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="486"/>
        <source>Scan subvolumes</source>
        <translation>Nach Untersektionen suchen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="514"/>
        <source>New &amp;layout</source>
        <translation>Neue Auftei&amp;lung</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="515"/>
        <source>&amp;Reset layout</source>
        <translation>&amp;Aufteilung zurücksetzen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="538"/>
        <source>Default subvolume</source>
        <translation>Standard-Subvolumen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="539"/>
        <source>Remove subvolume</source>
        <translation>Untersektion entfernen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="621"/>
        <source>Unlock Drive</source>
        <translation>Laufwerk entschlüsseln</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="626"/>
        <source>Password:</source>
        <translation>Paßwort:</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="652"/>
        <source>Could not unlock device. Possible incorrect password.</source>
        <translation>Das Gerät konnte nicht entschlüsselt werden. Falsches Paßwort?</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="680"/>
        <source>Failed to close %1</source>
        <translation>%1 konnte nicht geschlossen werden</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="724"/>
        <source>Invalid subvolume label</source>
        <translation>Ungültige Bezeichnung der Untersektion</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="733"/>
        <source>Duplicate subvolume label</source>
        <translation>Bezeichnung der Untersektion ist doppelt vorhanden</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="742"/>
        <source>Invalid use for %1: %2</source>
        <translation>Falsche Verwendung von %1: %2</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="753"/>
        <source>%1 is already selected for: %2</source>
        <translation>%1 wurde bereits für %2 ausgewählt.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="767"/>
        <source>A root partition of at least %1 is required.</source>
        <translation>Eine root-Partition von mindestens %1 ist erforderlich.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="773"/>
        <source>Cannot preserve /home inside root (/) if a separate /home partition is also mounted.</source>
        <translation>Das /home-Verzeichnis kann nicht als Unterverzeichnis des Stammverzeichnisses (/) erhalten bleiben, wenn zugleich eine separate Partition als /home-Verzeichnis eingehängt wird.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="789"/>
        <source>Reuse (no reformat) %1</source>
        <translation>%1 wiederverwenden (keine Neuformatierung)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="792"/>
        <source>Format %1</source>
        <translation>Format %1</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="808"/>
        <source>Reuse subvolume %1 as %2</source>
        <translation>Subvolume %1 als %2 wiederverwenden</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="810"/>
        <source>Delete subvolume %1</source>
        <translation>Subvolume %1 löschen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="813"/>
        <source>Overwrite subvolume %1</source>
        <translation>Subvolume %1 überschreiben</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="814"/>
        <source>Overwrite subvolume %1 to use for %2</source>
        <translation>Subvolume %1 zur Verwendung für %2 überschreiben</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="816"/>
        <source>Create subvolume %1</source>
        <translation>Subvolume %1 erstellen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="817"/>
        <source>Create subvolume %1 to use for %2</source>
        <translation>Subvolume %1 zur Verwendung für %2 erstellen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="832"/>
        <source>You must choose a separate boot partition when encrypting root.</source>
        <translation>Für die Verschlüsselung der root-Partition ist eine eigene boot-Partition erforderlich.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="783"/>
        <source>Prepare %1 partition table on %2</source>
        <translation>Partitionstabelle %1 auf %2 vorbereiten</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="793"/>
        <source>Format %1 to use for %2</source>
        <translation>Formatiere %1 zur Verwendung für %2</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="794"/>
        <source>Reuse (no reformat) %1 as %2</source>
        <translation>Verwende %1 als %2 wieder, ohne Neuformatierung.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="795"/>
        <source>Delete the data on %1 except for /home, to use for %2</source>
        <translation>Lösche zur Nutzung als %2 alle auf %1 vorhandenen Daten, außer dem Verzeichnis /home</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="798"/>
        <source>Create %1 without formatting</source>
        <translation>Erstelle %1 ohne Formatierung</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="799"/>
        <source>Create %1, format to use for %2</source>
        <translation>Erstelle %1, formatiere zur Verwendung für %2</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="948"/>
        <source>The following drives are, or will be, setup with GPT, but do not have a BIOS-GRUB partition:</source>
        <translation>Die folgenden Laufwerke sind oder werden mit »GPT« formatiert, haben aber keine »BIOS-GRUB« Partition:</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="950"/>
        <source>This system may not boot from GPT drives without a BIOS-GRUB partition.</source>
        <translation>Dieses System kann möglicherweise nicht von mit »GPT« formatierten Laufwerken gebootet werden, wenn keine »BIOS-GRUB« Partition vorhanden ist.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="833"/>
        <location filename="../partman.cpp" line="907"/>
        <location filename="../partman.cpp" line="925"/>
        <location filename="../partman.cpp" line="951"/>
        <source>Are you sure you want to continue?</source>
        <translation>Sind sie sicher, daß sie fortfahren möchten?</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="517"/>
        <source>Layout &amp;Builder...</source>
        <translation>Layout &amp;Erstellung...</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="879"/>
        <source>%1 (%2) requires %3</source>
        <translation>%1 (%2) benötigt %3</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="905"/>
        <source>The installation may fail because the following volumes are too small:</source>
        <translation>Die Installation könnte fehlschlagen, weil die folgenden Datenträger zu klein sind:</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="839"/>
        <source>The %1 installer will now perform the requested actions.</source>
        <translation>Das %1-Installationsprogramm wird nun die gewünschten Aktionen durchführen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="840"/>
        <source>These actions cannot be undone. Do you want to continue?</source>
        <translation>Diese Aktion kann nicht rückgängig gemacht werden. Möchten sie fortfahren?</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="918"/>
        <source>This system uses EFI, but no valid EFI system partition was assigned to /boot/efi separately.</source>
        <translation>Dieses System verwendet EFI, aber /boot/efi wurde keine gültige EFI-Systempartition separat zugewiesen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="921"/>
        <source>The volume assigned to /boot/efi is not a valid EFI system partition.</source>
        <translation>Der Datenträger, der /boot/efi zugewiesen ist, ist keine gültige EFI-Systempartition.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="988"/>
        <source>The disks with the partitions you selected for installation are failing:</source>
        <translation>Die Festplatten mit den für die Installtion gewählten Partitionen ist fehlerhaft :</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="992"/>
        <source>Smartmon tool output:</source>
        <translation>Ausgabe des Prüfprogramms Smartmon :</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="993"/>
        <source>The disks with the partitions you selected for installation pass the SMART monitor test (smartctl), but the tests indicate it will have a higher than average failure rate in the near future.</source>
        <translation>Die Laufwerke mit den Partitionen, die sie für die Installation gewählt haben, haben den S.M.A.R.T. Monitor Test (smartctl) bestanden, aber die Tests lassen in naher Zukunft eine überdurchschnittliche Ausfallwahrscheinlichkeit erwarten. </translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="998"/>
        <source>If unsure, please exit the Installer and run GSmartControl for more information.</source>
        <translation>Im Zweifel brechen sie bitte die Installation ab und führen GSmartControl aus, um weitere Informationen zu erhalten.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1000"/>
        <source>Do you want to abort the installation?</source>
        <translation>Wollen sie die Installation abbrechen ?</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1005"/>
        <source>Do you want to continue?</source>
        <translation>Möchten sie fortfahren?</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1211"/>
        <source>Failed to format LUKS container.</source>
        <translation>Formatieren des LUKS-Containers fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1231"/>
        <source>Failed to open LUKS container.</source>
        <translation>Öffnen des LUKS-Containers fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1092"/>
        <source>Failed to prepare required partitions.</source>
        <translation>Erforderliche Partitionen konnten nicht erfolgreich vorbereitet werden.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1131"/>
        <source>Preparing partition tables</source>
        <translation>Vorbereitung der Partitionstabellen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1148"/>
        <source>Preparing required partitions</source>
        <translation>Bereite benötigte Partitionen vor</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1216"/>
        <source>Creating encrypted volume: %1</source>
        <translation>Anlegen des verschlüsselten Laufwerkes: %1</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1251"/>
        <source>Formatting: %1</source>
        <translation>Formatieren: %1</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1244"/>
        <source>Failed to format partition.</source>
        <translation>Formatieren der Partition fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1309"/>
        <source>Failed to prepare subvolumes.</source>
        <translation>Vorbereiten der Subvolumes fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1310"/>
        <source>Preparing subvolumes</source>
        <translation>Vorbereitung der Untersektionen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1408"/>
        <source>Failed to mount partition.</source>
        <translation>Mount der Partition fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="1413"/>
        <source>Mounting: %1</source>
        <translation>Einhängen: %1</translation>
    </message>
</context>
<context>
    <name>PartMan::ItemDelegate</name>
    <message>
        <location filename="../partman.cpp" line="2457"/>
        <source>&amp;Templates</source>
        <translation>&amp;Vorlagen</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="2465"/>
        <source>Compression (Z&amp;STD)</source>
        <translation>Komprimierung (Z&amp;STD)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="2467"/>
        <source>Compression (&amp;LZO)</source>
        <translation>Kompression (mit &amp;LZO)</translation>
    </message>
    <message>
        <location filename="../partman.cpp" line="2469"/>
        <source>Compression (&amp;ZLIB)</source>
        <translation>Kompression (mit &amp;ZLIB)</translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="../passedit.cpp" line="144"/>
        <source>Negligible</source>
        <translation>Minimal</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="144"/>
        <source>Very weak</source>
        <translation>Sehr schwach</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="144"/>
        <source>Weak</source>
        <translation>Schwach</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="145"/>
        <source>Moderate</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="145"/>
        <source>Strong</source>
        <translation>Stark</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="145"/>
        <source>Very strong</source>
        <translation>Sehr stark</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="147"/>
        <source>Password strength: %1</source>
        <translation>Passwortstärke: %1</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="180"/>
        <source>Hide the password</source>
        <translation>Paßwort verbergen</translation>
    </message>
    <message>
        <location filename="../passedit.cpp" line="180"/>
        <source>Show the password</source>
        <translation>Paßwort anzeigen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../app.cpp" line="89"/>
        <source>Customizable GUI installer for MX Linux and antiX Linux</source>
        <translation>Konfigurierbares Installationsprogramm mit grafischer Benutzeroberfläche (GUI) für MX Linux und antiX Linux</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="92"/>
        <source>Installs automatically using the configuration file (more information below).
-- WARNING: potentially dangerous option, it will wipe the partition(s) automatically.</source>
        <translation>Automatische Installation auf Basis einer Konfigurationsdatei. (Details siehe unten).
-- VORSICHT !  Diese Option löscht Partitionen ohne Rückfrage automatisch und ist daher riskant.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="94"/>
        <source>Overrules sanity checks on partitions and drives, causing them to be displayed.
-- WARNING: this can break things, use it only if you don&apos;t care about data on drive.</source>
        <translation>Integritätsprüfungen für Partitionen und Laufwerke außer Kraft setzen, sodaß sie angezeigt werden.
VORSICHT: Dies kann Schäden verursachen. Bitte nur aktivieren, wenn ein möglicher Datenverlust akzeptabel ist.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="96"/>
        <source>Load a configuration file as specified by &lt;config-file&gt;.
By default /etc/minstall.conf is used.
This configuration can be used with --auto for an unattended installation.
The installer creates (or overwrites) /mnt/antiX/etc/minstall.conf and saves a copy to /etc/minstalled.conf for future use.
The installer will not write any passwords or ignored settings to the new configuration file.
Please note, this is experimental. Future installer versions may break compatibility with existing configuration files.</source>
        <translation>Einlesen einer unter &lt;config-file&gt; angegeben Konfigurationsdatei.
Als Standard wird /etc/minstall.conf verwendet.
Diese Konfiguration kann mit der Option --auto für eine unbeaufsichtigte Installation genutzt werden.
Das Installationsprogramm legt die Datei /mnt/antiX/etc/minstall.conf an bzw. überschreibt sie sofern vorhanden, und erzeugt eine Kopie /etc/minstalled.conf zur späteren Verwendung.
Weder Paßworte noch abgewählte Einstellungen werden in die Konfigurationsdateien aufgenommen.
Zur Beachtung: Diese Funktion ist experimentell. Zukünftige Versionen des Installationsprogramms können inkompatibel zu Konfigurationsdateien früherer Versionen sein.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="102"/>
        <source>Shutdown automatically when done installing.</source>
        <translation>Nach der Installation automatisch abschalten.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="103"/>
        <source>Always use GPT when doing a whole-drive installation regardlesss of capacity.
Without this option, GPT will only be used on drives with at least 2TB capacity.
GPT is always used on whole-drive installations on UEFI systems regardless of capacity, even without this option.</source>
        <translation>Diese Option aktiviert die Verwendung einer “Globally-Unique-Identifier” Partitionstabelle (GPT) anstelle einer “Master-Boot-Record” Partitionstabelle (MBR), unabhängig von der Laufwerksgröße, sofern das gesamte Laufwerk für die Installation zur Verfügung steht.
Standardmäßig wird GPT nur für Laufwerke mit einer Größe von mehr als 2 TB verwendet.
Auf Systemen mit “vereinheitlichter erweiterbarer Firmware-Schnittstelle“ (UEFI) wird GPT unabhängig von der Laufwerksgröße verwendet, auch wenn diese Option nicht aktiviert ist.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="106"/>
        <source>Do not unmount /mnt/antiX or close any of the associated LUKS containers when finished.</source>
        <translation>Hängen sie /mnt/antiX nicht aus und schließen auch keinen der zugeordneten »LUKS«-Container nach Fertigstellung!</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="107"/>
        <source>Another testing mode for installer, partitions/drives are going to be FORMATED, it will skip copying the files.</source>
        <translation>Ein zusätzlicher Testmodus des Installationsprogramms: Die Partitionen bzw. Laufwerke werden FORMATIERT, das Kopieren der Dateien übersprungen.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="108"/>
        <source>Install the operating system, delaying prompts for user-specific options until the first reboot.
Upon rebooting, the installer will be run with --oobe so that the user can provide these details.
This is useful for OEM installations, selling or giving away a computer with an OS pre-loaded on it.</source>
        <translation>Installation des Betriebssystems. Optionen zur individualisierten Konfiguration werden erst nach dem ersten Neustart abgefragt.
Beim Neustart wird das Installationsprogramm automatisch erneut, jedoch mit der Option --oobe gestartet, sodaß der Nutzer die noch fehlenden Einstellungen vornehmen kann.
Diese Option ist insbesondere für OEM-Installationen geeignet, wenn ein Computer mit vorinstalliertem Betriebssystem bereitgestellt werden soll.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="111"/>
        <source>Out Of the Box Experience option.
This will start automatically if installed with --oem option.</source>
        <translation>Option zur Vorinstallation des Betriebssystems.
Automatischer Start, sofern mit der Option --oem installiert.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="113"/>
        <source>Test mode for GUI, you can advance to different screens without actially installing.</source>
        <translation>Testmodus für die grafische Benutzeroberfläche (GUI). Verschiedene Oberflächen sind ohne endgültige Installation zugänglich.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="114"/>
        <source>Reboots automatically when done installing.</source>
        <translation>Automatischer Reboot nach Abschluss der Installation.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="115"/>
        <source>Installing with rsync instead of cp on custom partitioning.
-- doesn&apos;t format /root and it doesn&apos;t work with encryption.</source>
        <translation>Installation mithilfe des Datensynchronisationsprogramms “rsync” anstelle des Kopierbefehls “cp” auf angepaßter Festplattenpartitionierung.
-- Das Verzeichnis /root wird nicht formatiert, und die Option kann nicht mit Verschlüsselung verwendet werden.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="117"/>
        <source>Always check the installation media at the beginning.</source>
        <translation>Das Installationsmedien immer zuerst überprüfen</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="118"/>
        <source>Do not check the installation media at the beginning.
Not recommended unless the installation media is guaranteed to be free from errors.</source>
        <translation>Das Installationsmedium nicht zuerst überprüfen.
Das wird nicht empfohlen, es sei denn, das Installationsmedium ist garantiert fehlerfrei.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="122"/>
        <source>Load a configuration file as specified by &lt;config-file&gt;.</source>
        <translation>Lade die durch &lt;config-file&gt; bezeichnete Konfigurationsdatei.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="126"/>
        <source>Too many arguments. Please check the command format by running the program with --help</source>
        <translation>Zu viele Argumente. Bitte überprüfen sie das Befehlsformat, indem sie das Programm mit --help ausführen</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="131"/>
        <source>%1 Installer</source>
        <translation>%1 Installationsprogramm</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="139"/>
        <source>The installer won't launch because it appears to be running already in the background.

Please close it if possible, or run &apos;pkill minstall&apos; in terminal.</source>
        <translation>Das Installations-Programm kann nicht erneut gestartet werden, weil es scheinbar bereits im Hintergrund läuft.

Falls möglich, beenden sie es bitte, oder führen &apos;pkill minstall&apos; im Terminal aus.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="146"/>
        <source>This operation requires root access.</source>
        <translation>Dieser Vorgang erfordert Administratorrechte.</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="167"/>
        <source>Configuration file (%1) not found.</source>
        <translation>Konfigurationsdatei (%1) nicht gefunden.</translation>
    </message>
</context>
<context>
    <name>SwapMan</name>
    <message>
        <location filename="../swapman.cpp" line="85"/>
        <source>Failed to create or install swap file.</source>
        <translation>Die Auslagerungsdatei konnte nicht erstellt oder installiert werden.</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="91"/>
        <source>Creating swap file</source>
        <translation>Auslagerungsdatei erstellen</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="103"/>
        <source>Configuring swap file</source>
        <translation>Auslagerungsdatei konfigurieren</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="67"/>
        <source>Invalid location</source>
        <translation>Ungültiger Speicherort</translation>
    </message>
    <message>
        <location filename="../swapman.cpp" line="70"/>
        <source>Maximum: %1 MB</source>
        <translation>Maximum: %1 MB</translation>
    </message>
</context>
</TS>